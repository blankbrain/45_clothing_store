package com.ClothingStorePOS.shiro;

import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoPermissionExceptionResolver implements HandlerExceptionResolver {
    /**
     *
     * 捕获无权限异常，跳转到无权限提示页面
     *
     */


        public ModelAndView resolveException(HttpServletRequest request,
                                             HttpServletResponse response, Object handler, Exception ex) {
            // TODO Auto-generated method stub
            System.out.println("==============异常开始=============");
            //如果是shiro无权操作，因为shiro 在操作auno等一部分不进行转发至无权限url
            if(ex instanceof UnauthorizedException){
                ModelAndView mv = new ModelAndView("/unAuth");
                return mv;
            }
            ex.printStackTrace();
            System.out.println("==============异常结束=============");
            ModelAndView mv = new ModelAndView("/unAuth");
            mv.addObject("exception", ex.toString().replaceAll("\n", "<br/>"));
            return mv;
        }



}
