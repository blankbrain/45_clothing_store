package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.CategoryKey;
import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.pojo.OrderItem;
import com.ClothingStorePOS.services.ICategoryService;
import com.ClothingStorePOS.services.IStatementService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/saleStatement")
public class SaleStatementController {

    @Resource(name = "categoryService")
    private ICategoryService categoryService;
    @Resource(name = "statementService")
    private IStatementService statementService;

    /**
     * 获取所有类别
     * @return
     */
    @RequiresPermissions("saleStatement:getCategory")
    @RequestMapping("/getCategory")
    @ResponseBody
    public Object getCategory(){

        List<CategoryKey> categoryList = this.categoryService.getAllCategory();
        Map map = new HashMap();

        //按照layui的格式存储数据
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", categoryList.size());
        map.put("data", categoryList);

        //转化为json格式
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(map));
        System.out.println(jsonObject);
        return jsonObject;
    }

    /**
     * 根据类别获取服装
     * @return
     */
    @RequiresPermissions("saleStatement:getClothesByCategory")
    @RequestMapping("/getClothesByCategory")
    @ResponseBody
    public Object getClothesByCategory(String clothesType){
        List<Clothes> clothesList = this.statementService.searchClothesByCategory(clothesType);
/*        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesList));*/
        JSONArray array= JSONArray.parseArray(JSONObject.toJSONString(clothesList));
        System.out.println(array);
        return array;
    }

    /**
     * 根据起始时间获取指定种类服装的销售量
     * @param dateRange
     * @param clothesType
     * @return
     * @throws ParseException
     */
    @RequiresPermissions("saleStatement:saleVolumeDateRange")
    @RequestMapping("/saleVolumeDateRange")
    @ResponseBody
    public Object saleVolumeDateRange(String dateRange,String clothesType) throws ParseException {
        System.out.println("时间范围为："+dateRange);
        //获取并转换起始和结束时间
        int k = dateRange.indexOf(' ');
        String s1 = dateRange.substring(0,k);
        String s2 = dateRange.substring(k+3);
        System.out.println(s2);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = dateFormat.parse(s1);
        Date endDate = dateFormat.parse(s2);

        List<Clothes> clothesIdList = this.statementService.searchClothesByCategory(clothesType);//获取服装编号
        List<OrderItem> orderItemList = this.statementService.saleVolumeDateRange(startDate,endDate,clothesType);

        Map resultMap = new HashMap();

        for (Clothes clothes:
             clothesIdList) {
            int num = 0;
            for (OrderItem orderItem:
                 orderItemList) {
                if (clothes.getClothesId().equals(orderItem.getClothesid())){
                      num += orderItem.getNum();
                }
                 resultMap.put(clothes.getClothesName(),num);
            }
        }
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(resultMap));
        return jsonObject;

    }

    /**
     * 获取指定服装的销售数量
     * @param clothesId
     * @param year
     * @return
     * @throws ParseException
     */
    @RequiresPermissions("saleStatement:saleVolumeMonthRange")
    @RequestMapping("/saleVolumeMonthRange")
    @ResponseBody
    public Object saleVolumeMonthRange(String clothesId,String year) throws ParseException {
        String s1 = year+"-01";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date month = dateFormat.parse(s1);
        System.out.println("初始月份为"+month);
        ArrayList numList = new ArrayList();
        for (int i = 0;i<12;i++){
            int num = 0;
            List<OrderItem> orderItemList = this.statementService.saleVolumeMonthRange(clothesId,month);
            for (OrderItem oi:
                 orderItemList) {
                num += oi.getNum();
            }
            numList.add(num);
            month = getAddMonth(month,1);
/*            System.out.println("月份为"+month);*/
        }
        JSONArray resultArray= JSONArray.parseArray(JSONObject.toJSONString(numList));
        System.out.println(resultArray);
        return resultArray;
    }

    /**
     * 转换为季度范围
     * @param year
     * @return
     * @throws ParseException
     */
    @RequiresPermissions("saleStatement:turnoverMonthRange")
    @RequestMapping("/turnoverMonthRange")
    @ResponseBody
    public Object turnoverMonthRange(String year) throws ParseException {
        String s1 = year+"-01";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date startMonth = dateFormat.parse(s1);
        System.out.println("初始月份为"+startMonth);
        ArrayList amountList = new ArrayList();
        for (int i = 0;i<4;i++){
            double totalAmount = this.statementService.turnoverMonthRange(startMonth,getAddMonth(startMonth,3));
            amountList.add(totalAmount);
            startMonth = getAddMonth(startMonth,3);
            /*            System.out.println("月份为"+month);*/
        }
        JSONArray resultArray= JSONArray.parseArray(JSONObject.toJSONString(amountList));
        System.out.println(resultArray);
        return resultArray;
    }


    //月份递增
    public Date getAddMonth(Date date,int monthAmount){
/*        Calendar curr = Calendar.getInstance();
        curr.set(Calendar.MONTH,curr.get(Calendar.MONTH)+month); //增加一月
        Date date=curr.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowStr = sdf.format(date);*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, monthAmount);
        return calendar.getTime();
    }


}
