package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.Order;
import com.ClothingStorePOS.pojo.OrderItem;
import com.ClothingStorePOS.pojo.Result;
import com.ClothingStorePOS.services.ISaleService;
import com.ClothingStorePOS.shiro.ShiroUser;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Resource(name = "saleService")
    private ISaleService saleService;

    /**
     * 查询所有挂单订单
     */
    @RequiresPermissions("order:findTakeOrder")
    @RequestMapping("/findTakeOrder")
    @ResponseBody
    public Object getTakeOrder(HttpServletRequest request){
        //获取搜索的限制条件
        String keyWord = request.getParameter("keyWord");

        System.out.println(keyWord);
        Map takeOrderMap = new HashMap();

        if(keyWord==null||keyWord=="") {
            List<Order> orderList = this.saleService.getAllTakeOrder();

            //按照layui的格式存储数据
            takeOrderMap.put("code", 0);
            takeOrderMap.put("msg", "");
            takeOrderMap.put("count", orderList.size());
            takeOrderMap.put("data", orderList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(takeOrderMap));
            return jsonObject;
        }else {
            List<Order> orderList = this.saleService.selectTakeOrderByPhone(keyWord);
            System.out.println("手机尾号为："+keyWord);
            //按照layui的格式存储数据
            takeOrderMap.put("code", 0);
            takeOrderMap.put("msg", "");
            takeOrderMap.put("count", orderList.size());
            takeOrderMap.put("data", orderList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(takeOrderMap));
            return jsonObject;

        }

    }

    /**
     * 删除选中的挂单
     */
    @RequestMapping("/deletePendingOrder")
    @ResponseBody
    public int deletePendingOrder(Long id){
        this.saleService.deletePendingOrder(id);
        return this.saleService.deleteByOrderId(id);
    }

    /**
     * 批量删除订单
     */
    @RequestMapping("/batchDelPendingOrder")
    @ResponseBody
    public int batchDelPendingOrder(HttpServletRequest request){
        String str = request.getParameter("str");
        String a[] = str.split(",");
        Long arr[] = new Long[a.length];
        for (int i=0;i<a.length;i++){
            arr[i] = Long.parseLong(a[i]);
            System.out.println(arr[i]);
        }
        return this.saleService.batchDelPendingOrder(arr);
    }

    /**
     * 取单
     * @param orderId
     * @param session
     * @return
     */
    @RequestMapping("/takePendingOrder")
    @ResponseBody
    public Result takePendingOrder(Long orderId, HttpSession session){
        System.out.println("查询的订单号为"+orderId);
        //获取订单
        Order order = this.saleService.selectOrderById(orderId);
        System.out.println("订单总金额为："+order.getOrderItemList().size());
        Result result = new Result();
        result.setSuccess(true);
        //修改员工号
        ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        order.setCashierid(user.loginName);
        ArrayList<OrderItem> orderItemArrayList = order.getOrderItemList();
        session.setAttribute("ORDER_SESSION", order);
        session.setAttribute("OrderItemList_SESSION",orderItemArrayList);
        System.out.println("取单的订单清单"+orderItemArrayList.get(0));

        return result;


    }

    /**
     * 判断当前OrderItemList_SESSION是否存在
     * @param
     * @return
     */
    @RequestMapping("/seekSession")
    @ResponseBody
    public Result seekSession(HttpSession session){
        Result result = new Result();
        if (session.getAttribute("OrderItemList_SESSION")==null)
            result.setSuccess(false);
        else
            result.setSuccess(true);
        return result;
    }
}
