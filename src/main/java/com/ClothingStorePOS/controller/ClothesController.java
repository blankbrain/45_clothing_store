package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.pojo.Stock;
import com.ClothingStorePOS.services.IClothesService;
import com.ClothingStorePOS.services.IStockService;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/clothes")
public class ClothesController {

    @Resource(name = "clothesService")
    private IClothesService clothesService;
    @Resource(name ="stockService" )
    private IStockService stockService;

    //获取所有商品信息
    @RequiresPermissions("clothes:find")
    @RequestMapping("/find")
    @ResponseBody
    public Object getAllClothes(HttpServletRequest request){

        //当前页数
        int page = Integer.parseInt(request.getParameter("page"));
        //每页显示条数
        int limit = Integer.parseInt(request.getParameter("limit"));

        int start = limit*(page-1);

        //获取搜索的限制条件
        String keyWord = request.getParameter("keyWord");
        String keyType = request.getParameter("keyType");

        System.out.println(keyType);
        System.out.println(keyWord);
        Map clothesMap = new HashMap();

        if(keyWord==null||keyWord==""||keyType==null||keyType=="") {
            List<Clothes> clothesList = this.clothesService.getAllClothes(start, limit);
            int counts = this.clothesService.getClothesCounts();

            for (Clothes clo:
                    clothesList) {
                Stock stock = this.stockService.selectByClothesId(clo.getClothesId());
                clo.setStock(stock.getStock());
            }

            //按照layui的格式存储数据
            clothesMap.put("code", 0);
            clothesMap.put("msg", "");
            clothesMap.put("count", counts);
            clothesMap.put("data", clothesList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesMap));
            return jsonObject;
        }else {
            List<Clothes> clothesList = this.clothesService.searchClothes(keyWord,keyType,start, limit);

            for (Clothes clo:
                    clothesList) {
                Stock stock = this.stockService.selectByClothesId(clo.getClothesId());
                clo.setStock(stock.getStock());
            }
            //按照layui的格式存储数据
            clothesMap.put("code", 0);
            clothesMap.put("msg", "");
            clothesMap.put("count", clothesList.size());
            clothesMap.put("data", clothesList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesMap));
            return jsonObject;

/*
            List<Clothes> clothesList = new ArrayList<>();
            Clothes clothes = new Clothes();
                if((keyWord!=null||keyWord!="")&&keyType.equals("clothesId")){
                    clothes.setClothesId(keyWord);
                    clothesList = this.clothesService.getClothesById(clothes,start, limit);
                }
                if ((keyWord!=null||keyWord!="")&&keyType.equals("clothesName")){
                    clothes.setClothesName(keyWord);
                     clothesList = this.clothesService.getClothesByName(clothes,start, limit);
                }

            //按照layui的格式存储数据
            clothesMap.put("code", 0);
            clothesMap.put("msg", "");
            clothesMap.put("count", clothesList.size());
            clothesMap.put("data", clothesList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesMap));
            return jsonObject;
*/

        }

    }

    //编辑商品信息
    @RequiresPermissions("clothes:edit")
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public int editClothes(Clothes clothes){
        clothes.caldiscountPrice();
        Stock stock = new Stock(clothes.getClothesId(),clothes.getStock());
        this.stockService.updateByClothesId(stock);
        return this.clothesService.editClothes(clothes);
    }

    //删除选定单行的数据
    @RequiresPermissions("clothes:delete")
    @RequestMapping("/delete")
    @ResponseBody
    public int deleteSelectOne(@RequestParam("clothesId")String clothesId) {
        return this.clothesService.deleteSelectOne(clothesId);
    }

    //批量删除信息
    @RequiresPermissions("clothes:batchDelete")
    @RequestMapping("/batchDelete")
    @ResponseBody
    public int batchDelete(HttpServletRequest request){
        String str = request.getParameter("str");
        String a[] = str.split(",");
        String arr[] = new String[a.length];
        for (int i=0;i<a.length;i++){
            arr[i] = a[i];
            System.out.println(arr[i]);
        }
        return this.clothesService.batchDelete(arr);
    }

    //新增服装
    @RequiresPermissions("clothes:add")
    @RequestMapping(value = "/add")
    @ResponseBody
    public int addClothes(Clothes clothes){
        clothes.caldiscountPrice();
        return this.clothesService.addClothes(clothes);

    }

    //修改商品页面弹窗
    @RequiresPermissions("clothes:toAlterClothesPage")
    @RequestMapping("/toAlterClothesPage")
    public String toAlterClothesPage(String clothesId,HttpServletRequest request){
        Clothes clothes = this.clothesService.searchById(clothesId);
        clothes.setStock(this.stockService.selectByClothesId(clothesId).getStock());
        request.setAttribute("clothes",clothes);
        return "/alterClothes";
    }

}
