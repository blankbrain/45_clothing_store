package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.UserVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    // 返回首页
    @RequiresPermissions("/index")
    @RequestMapping("/index")
    public  String  index(){
        return "/index";
    }
    // 后台主页面
    @RequestMapping("/main")
    public  String  main(){
        return "/main" ;
    }

    // 登录页面
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public  String  login(){
        return "/login" ;
    }

    //跳转到商品管理页面
    @RequiresPermissions("page:toClothesPage")
    @RequestMapping("/toClothesPage")
    public String toClothesPage(){
        return "/goodsPage";
    }

    //跳转到类别管理页面
    @RequiresPermissions("page:toCategoryPage")
    @RequestMapping("/toCategoryPage")
    public String toCategoryPage(){
        return "/categoryPage";
    }

    //跳转到添加用户页面
    @RequiresPermissions("page:toAddUserPage")
    @RequestMapping("/toAddUserPage")
    public String toAddUserPage(){
        return "/addUser";
    }

    //跳转到用户信息页面
    @RequiresPermissions("page:toUserPage")
    @RequestMapping("/toUserPage")
    public String toUserPage(){
        return "/userPage";
    }

    //跳转到商品销售页面
    @RequiresPermissions("page:toSalePage")
    @RequestMapping("/toSalePage")
    public String toSalePage(){
        return "/salePage";
    }

    //跳转到取单页面
    @RequiresPermissions("page:toTakeOrderPage")
    @RequestMapping("/toTakeOrderPage")
    public String toTakeOrderPage(){
        return "/takeOrder";
    }

    //跳转到销售统计页面
    @RequiresPermissions("page:toSaleDisplayPage")
    @RequestMapping("/toSaleDisplayPage")
    public String toSaleDisplayPage(){
        return "/saleDisplayPage";
    }

    //跳转到入库页面
    @RequiresPermissions("page:toStockInPage")
    @RequestMapping("/toStockInPage")
    public String toStockInPage(){
        return "/stockInPage";
    }

    /**
     * 未授权
     */

    @RequestMapping("/unAuth")
    public String unAuth() {
        if (SecurityUtils.getSubject().isAuthenticated() == false) {
            return "redirect:/login";
        }
        return "/unAuth";
    }
}
