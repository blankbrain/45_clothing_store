package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.CategoryKey;
import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.services.ICategoryService;
import com.ClothingStorePOS.services.IClothesService;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Resource(name = "categoryService")
    private ICategoryService categoryService;
    @Resource(name = "clothesService")
    private IClothesService clothesService;

    //获取所有类型
    @RequiresPermissions("category:find")
    @RequestMapping("/find")
    @ResponseBody
    public Object getAllCategory(){
        Map categoryMap = new HashMap();

        List<CategoryKey> categoryList = this.categoryService.getAllCategory();

        //按照layui的格式存储数据
        categoryMap.put("code", 0);
        categoryMap.put("msg", "");
        categoryMap.put("count", categoryList.size());
        categoryMap.put("data", categoryList);

        //转化为json格式
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(categoryMap));
        System.out.println(jsonObject);
        return jsonObject;
    }

    //获取相应类型的衣服
    @RequiresPermissions("category:getAll")
    @RequestMapping("/getAll")
    @ResponseBody
    public Object getAll(HttpServletRequest request){
        //当前页数
        int page = Integer.parseInt(request.getParameter("page"));
        //每页显示条数
        int limit = Integer.parseInt(request.getParameter("limit"));

        int start = limit*(page-1);

        //获取搜索的限制条件
        String keyWord = request.getParameter("keyWord");

        System.out.println(keyWord);
        Map clothesMap = new HashMap();

        if(keyWord==null||keyWord=="") {
            List<Clothes> clothesList = this.categoryService.getAll(start, limit);
            int counts = this.clothesService.getClothesCounts();

            //按照layui的格式存储数据
            clothesMap.put("code", 0);
            clothesMap.put("msg", "");
            clothesMap.put("count", counts);
            clothesMap.put("data", clothesList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesMap));
            return jsonObject;
        }else {
            List<Clothes> clothesList = this.categoryService.searchByType(keyWord,start, limit);

            //按照layui的格式存储数据
            clothesMap.put("code", 0);
            clothesMap.put("msg", "");
            clothesMap.put("count", clothesList.size());
            clothesMap.put("data", clothesList);

            //转化为json格式
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(clothesMap));
            return jsonObject;

        }

    }

    //修改选中类别
    @RequiresPermissions("category:alterChoose")
    @RequestMapping("/alterChoose")
    @ResponseBody
    public int alterChoose(@RequestParam("type")String type,@RequestParam("oldType")String oldType){
        return this.categoryService.alterChoose(type,oldType);
    }

    //新增类别
    @RequiresPermissions("category:add")
    @RequestMapping("/add")
    @ResponseBody
    public int addCategory(@RequestParam("type")String type){
        return this.categoryService.addCategory(type);
    }


}
