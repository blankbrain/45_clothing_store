package com.ClothingStorePOS.controller;

import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.pojo.Result;
import com.ClothingStorePOS.pojo.Stock;
import com.ClothingStorePOS.services.IClothesService;
import com.ClothingStorePOS.services.IStockService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/stock")
public class StockController {
    @Resource(name = "stockService")
    private IStockService stockService;
    @Resource(name = "clothesService")
    private IClothesService clothesService;

    /**
     * 根据服装编号查找服装表
     * @param clothesId
     * @return
     */
    @RequiresPermissions("stock:selectClothesOne")
    @RequestMapping("/selectClothesOne")
    @ResponseBody
    public Clothes searchStock(String clothesId){
        Clothes clothes = this.clothesService.searchById(clothesId);
        return clothes;
    }

    /**
     * 新商品入库
     */
    @RequiresPermissions("stock:stockIn")
    @RequestMapping("/stockIn")
    @ResponseBody
    public Result stockIn(Clothes clothes){
        Result result = new Result();
        Stock stock = new Stock(clothes.getClothesId(),clothes.getStock());
        this.clothesService.addClothes(clothes);
        if (this.stockService.insertStock(stock)==1){
            result.setSuccess(true);
        }
        return result;
    }

    /**
     * 已存在的商品入库
     */
    @RequiresPermissions("stock:alterStock")
    @RequestMapping("/alterStock")
    @ResponseBody
    public Result alterStock(Stock stock){
        Result result = new Result();
        Stock s = this.stockService.selectByClothesId(stock.getClothesId());
        stock.setStock(stock.getStock()+s.getStock());
        if (this.stockService.updateByClothesId(stock)==1){
            result.setSuccess(true);
        }
        System.out.println("入库的库存是："+stock.getClothesId());
        return result;
    }

    /**
     * 根据商品编号查找同类型的商品库存
     */
    @RequiresPermissions("stock:searchStock")
    @RequestMapping("/searchStock")
    @ResponseBody
    public Object searchStock(Stock stock){
        List<Stock> stockList = new ArrayList<>();
        Clothes c = this.clothesService.searchById(stock.getClothesId());
        List<Clothes> clothesList = this.clothesService.selectSameName(c.getClothesName(),stock.getClothesSize());
        for (Clothes clothes:
             clothesList) {
            Stock s = this.stockService.selectByClothesId(clothes.getClothesId());
            s.setClothesName(clothes.getClothesName());
            s.setClothesSize(clothes.getClothesSize());
            s.setClothesColor(clothes.getClothesColor());
            stockList.add(s);
        }
        JSONArray result = JSONArray.parseArray(JSON.toJSONString(stockList));
        System.out.println(result);
        return result;
    }

}
