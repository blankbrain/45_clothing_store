package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleMapper {

    /**
     * 通过角色 Id 查找资源路径
     * @param roleId
     * @return
     */
    List<Map<Integer, String>> findRoleResourceListByRoleId(int roleId);

    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);
}