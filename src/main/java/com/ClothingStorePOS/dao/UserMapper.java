package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.User;
import com.ClothingStorePOS.pojo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

public interface UserMapper {

    User findUserByLoginName(String username);

/*    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);*/

    //添加用户
    int addUser(User user);

    List<User> findUserByRoleId(@Param("roleId")int roleId,@Param("start")int start,@Param("limit")int limit);

    int userCountsSale(@Param("roleId")int roleId);

    List<User> findAllUserByRoleId(@Param("roleId1") int roleId1, @Param("roleId2") int roleId2, @Param("start")int start, @Param("limit")int limit);

    int userCountsSaleAndManager(@Param("roleId1") int roleId1,@Param("roleId2")int roleId2);

    int deleteByUserId(int id);

    User selectUserById(@Param("id")Integer id);

    int editUser(UserVO userVO);

}