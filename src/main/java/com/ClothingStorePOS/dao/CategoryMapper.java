package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.CategoryKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("categoryMapper")
public interface CategoryMapper {

    List<CategoryKey> getAllCategory();


    int insert(CategoryKey record);

    int alterChoose(@Param("type") String type,@Param("oldType")String oldType);

    int addCategory(@Param("type") String type);

    int deleteByPrimaryKey(CategoryKey key);

    int insertSelective(CategoryKey record);
}