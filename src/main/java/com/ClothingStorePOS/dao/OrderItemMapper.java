package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Order;
import com.ClothingStorePOS.pojo.OrderItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("orderItemMapper")
public interface OrderItemMapper {

    int insertOrderItem(OrderItem orderItem);

    int deleteByOrderId(@Param("orderId")Long orderId);

    int updateOrderItem(OrderItem orderItem);

    List<OrderItem> saleVolumeDateRange(@Param("startDate")Date startDate,@Param("endDate")Date endDate,
                                        @Param("clothesType")String clothesType);

    List<OrderItem> saleVolumeMonthRange(@Param("clothesId")String clothesId,@Param("month")Date month);

    double turnoverMonthRange(@Param("startMonth")Date startMonth,@Param("endMonth")Date endMonth);

    int delByItemId(@Param("id")Long id);




    int deleteByPrimaryKey(Long id);

    int insert(OrderItem record);

    int insertSelective(OrderItem record);

    OrderItem selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderItem record);

    int updateByPrimaryKey(OrderItem record);
}