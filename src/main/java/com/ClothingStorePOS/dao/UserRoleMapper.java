package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.UserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserRoleMapper {

    List<Integer> findRoleIdListByUserId(int userId);

    int deleteByPrimaryKey(Integer id);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);

    //为用户添加角色
    int addUserRole(@Param("userId")Integer userId,@Param("roleId") Integer roleId);
}