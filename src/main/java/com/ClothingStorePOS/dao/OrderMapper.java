package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Order;
import com.ClothingStorePOS.pojo.OrderItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("orderMapper")
public interface OrderMapper {

    int insertNewOrder(Order order);

    List<Order> getAllTakeOrder();

    List<Order> selectTakeOrderByPhone(@Param("keyWord") String keyWord);

    int deletePendingOrder(@Param("id") Long id);

    int batchDelPendingOrder(Long[] id);

    Order selectOrderById(@Param("id") Long id);

    Order selectOrderByOrderId(@Param("id")Long id);

    int updateOrderByOrderId(Order order);





    int deleteByPrimaryKey(Long id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
}