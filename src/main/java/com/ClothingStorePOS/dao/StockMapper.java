package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Stock;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

public interface StockMapper {

    int insertStock(Stock stock);

    Stock selectByClothesId(@Param("clothesId") String clothesId);

    int updateByClothesId(Stock stock);




    int deleteByPrimaryKey(Integer id);

    int insert(Stock record);

    int insertSelective(Stock record);

    Stock selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Stock record);

    int updateByPrimaryKey(Stock record);
}