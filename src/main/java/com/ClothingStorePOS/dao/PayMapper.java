package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Pay;
import org.springframework.stereotype.Repository;

@Repository("payMapper")
public interface PayMapper {

    int insertPay(Pay pay);





    int deleteByPrimaryKey(Long id);

    int insert(Pay record);

    int insertSelective(Pay record);

    Pay selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Pay record);

    int updateByPrimaryKey(Pay record);
}