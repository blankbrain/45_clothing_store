package com.ClothingStorePOS.dao;

import com.ClothingStorePOS.pojo.Clothes;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("clothesMapper")
public interface ClothesMapper {


    List<Clothes> getAllClothes(@Param("start") int start, @Param("limit") int limit);

    int editClothes(Clothes clothes);

    int deleteSelectOne(String clothesId);

    int batchDelete(String clothesId[]);

    int addClothes(Clothes clothes);

    List<Clothes> searchClothes(@Param("keyWord") String keyWord, @Param("keyType") String keyType,@Param("start") int start, @Param("limit") int limit);

    List<Clothes> getClothesById(Clothes clothes,@Param("start") int start, @Param("limit") int limit);

    List<Clothes> getClothesByName(Clothes clothes,@Param("start") int start, @Param("limit") int limit);

    //类别管理中的按分类搜索
    List<Clothes> searchByType(@Param("keyWord") String keyWord, @Param("start") int start, @Param("limit") int limit);

    int getClothesCounts();

    Clothes searchById(@Param("clothesId")String clothesId);

    List<Clothes> searchClothesByCategory(@Param("clothesType")String clothesType);

    List<Clothes> selectSameName(@Param("clothesName")String clothesName,@Param("clothesSize")String clothesSize);











    int deleteByPrimaryKey(String clothesId);


    int insertSelective(Clothes record);

    Clothes selectByPrimaryKey(String clothesId);

    int updateByPrimaryKeySelective(Clothes record);

    int updateByPrimaryKey(Clothes record);
}