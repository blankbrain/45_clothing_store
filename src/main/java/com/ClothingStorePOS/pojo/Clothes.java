package com.ClothingStorePOS.pojo;

import java.math.BigDecimal;

public class Clothes {
    private String clothesId;

    private String clothesName;

    private String clothesType;

    private String clothesColor;

    private String clothesSize;

    private Double tagPrice;

    private int discount;

    private Double discountPrice;

    private String clothesMaterial;

    private int stock;

    public String getClothesId() {
        return clothesId;
    }

    public void setClothesId(String clothesId) {
        this.clothesId = clothesId == null ? null : clothesId.trim();
    }

    public String getClothesName() {
        return clothesName;
    }

    public void setClothesName(String clothesName) {
        this.clothesName = clothesName == null ? null : clothesName.trim();
    }

    public String getClothesType() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType = clothesType == null ? null : clothesType.trim();
    }

    public String getClothesColor() {
        return clothesColor;
    }

    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor == null ? null : clothesColor.trim();
    }

    public String getClothesSize() {
        return clothesSize;
    }

    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize == null ? null : clothesSize.trim();
    }

    public Double getTagPrice() {
        return tagPrice;
    }

    public void setTagPrice(Double tagPrice) {
        this.tagPrice = tagPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getClothesMaterial() {
        return clothesMaterial;
    }

    public void setClothesMaterial(String clothesMaterial) {
        this.clothesMaterial = clothesMaterial == null ? null : clothesMaterial.trim();
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void caldiscountPrice(){
        BigDecimal b = new BigDecimal(this.discount*0.1*this.tagPrice);
        this.discountPrice = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}