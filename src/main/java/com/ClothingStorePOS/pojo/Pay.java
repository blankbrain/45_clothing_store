package com.ClothingStorePOS.pojo;

import java.util.Date;

public class Pay {
    private Long id;

    private Long orderId;

    private Double amount;

    //支付方式默认是1现金支付，2是银联，3是微信，4是支付宝
    private Byte payStyle;

    private Date payDate;

    public Pay(){

    }

    public Pay(Long orderId,Double amount,Byte payStyle){
        this.orderId = orderId;
        this.amount = amount;
        this.payStyle = payStyle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Byte getPayStyle() {
        return payStyle;
    }

    public void setPayStyle(Byte payStyle) {
        this.payStyle = payStyle;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }
}