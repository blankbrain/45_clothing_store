package com.ClothingStorePOS.pojo;

public class Stock {
    private Integer id;

    private String clothesId;

    private String clothesName;

    private String clothesSize;

    private String clothesColor;

    private Integer stock;

    public Stock(){

    }

/*    public Stock(String clothesId,String clothesName,String clothesSize,String clothesColor,Integer stock){
        this.clothesId = clothesId;
        this.clothesName = clothesName;
        this.clothesSize = clothesSize;
        this.clothesColor = clothesColor;
        this.stock = stock;
    }*/
    public Stock(String clothesId,Integer stock){
        this.clothesId = clothesId;
        this.stock = stock;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesId() {
        return clothesId;
    }

    public void setClothesId(String clothesId) {
        this.clothesId = clothesId == null ? null : clothesId.trim();
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getClothesName() {
        return clothesName;
    }

    public void setClothesName(String clothesName) {
        this.clothesName = clothesName;
    }

    public String getClothesSize() {
        return clothesSize;
    }

    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize;
    }

    public String getClothesColor() {
        return clothesColor;
    }

    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor;
    }
}