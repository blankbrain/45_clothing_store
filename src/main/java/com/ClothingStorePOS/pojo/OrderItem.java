package com.ClothingStorePOS.pojo;

import java.math.BigDecimal;
import java.util.Objects;


public class OrderItem {

    private Long id;

    private Long orderid;

    private String clothesid;

    private Integer num;

    private Double price;

    private Double subtotal;

    private String clothesname;

    private Clothes clothes;

    public OrderItem(){

    }

    public OrderItem(Clothes clothes,int num){

        this.clothesid = clothes.getClothesId();
        this.clothesname = clothes.getClothesName();
        this.num = num;
        this.price = clothes.getDiscountPrice();
        BigDecimal b = new BigDecimal(num*price);
        this.subtotal = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public String getClothesid() {
        return clothesid;
    }

    public void setClothesid(String clothesid) {
        this.clothesid = clothesid == null ? null : clothesid.trim();
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Clothes getClothes() {
        return clothes;
    }

    public void setClothes(Clothes clothes) {
        this.clothes = clothes;
    }

    public String getClothesname() {
        return clothesname;
    }

    public void setClothesname(String clothesname) {
        this.clothesname = clothesname;
    }

    //计算小计
    public void calSubtotal(){
        BigDecimal b = new BigDecimal(this.num*this.price);
        this.subtotal = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItem)) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(id, orderItem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}