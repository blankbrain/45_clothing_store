package com.ClothingStorePOS.pojo;

import java.util.ArrayList;
import java.util.Date;

public class Order {
    private Long id;

    private String phone;

    //0代表该订单已完成，1代表未完成
    private Integer status;

    private Date buildTime;

    private String cashierid;

    private Double amount;

    private ArrayList<OrderItem> orderItemList;

    //private boolean isCompleted;
    public Order(){

    }

    public Order(String cashierid){
        this.cashierid = cashierid;
        this.orderItemList=new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(Date buildTime) {
        this.buildTime = buildTime;
    }

    public String getCashierid() {
        return cashierid;
    }

    public void setCashierid(String cashierid) {
        this.cashierid = cashierid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    //标记订单完成
    public void isCompleted(){
        this.status = 0;
    }

    //遍历订单列表计算总金额
    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
        double a = 0.0;
        for (OrderItem orderItem:
                orderItemList
             ) {
            a=a+orderItem.getSubtotal();
        }
        this.amount = a;
    }
}