package com.ClothingStorePOS.pojo;

import java.util.Date;
import java.util.List;

public class UserVO {

    public UserVO(){

    }

    public UserVO(int id,String loginname,String name,String password,String sex,byte age,Date createdate,String phone){
        this.id = id;
        this.loginname = loginname;
        this.name = name;
        this.password = password;
        this.sex = sex;
        if(sex.equals("0")){
            this.sex = "男";
        }else
            this.sex = "女";
        this.age = age;
        this.createdate = createdate;
        this.phone = phone;
    }

    private Integer id;

    private String loginname;

    private String name;

    private String password;

    private String sex;

    private Byte age;

    private Date createdate;

    private String phone;

    private String roleName;

    private Integer roleId;

    public Byte getAge() {
        return age;
    }

    public void setAge(Byte age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
