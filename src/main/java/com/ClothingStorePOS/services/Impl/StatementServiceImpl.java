package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.ClothesMapper;
import com.ClothingStorePOS.dao.OrderItemMapper;
import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.pojo.OrderItem;
import com.ClothingStorePOS.services.IStatementService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("statementService")
public class StatementServiceImpl implements IStatementService {

    @Resource(name = "clothesMapper")
    private ClothesMapper clothesMapper;
    @Resource(name = "orderItemMapper")
    private OrderItemMapper orderItemMapper;

    //根据服装类别获取服装id
    public List<Clothes> searchClothesByCategory(String clothesType){
        return this.clothesMapper.searchClothesByCategory(clothesType);
    }

    //根据起始日期和类别查找销售记录
    public List<OrderItem> saleVolumeDateRange(Date startDate, Date endDate, String clothesType){
        return this.orderItemMapper.saleVolumeDateRange(startDate,endDate,clothesType);
    }

    //根据月份查找指定商品的销售记录
    public List<OrderItem> saleVolumeMonthRange(String clothesId, Date month){
        return this.orderItemMapper.saleVolumeMonthRange(clothesId,month);
    }

    //计算不同季度的总营业额
    public double turnoverMonthRange(Date startMonth,Date endMonth){
        return this.orderItemMapper.turnoverMonthRange(startMonth,endMonth);
    }


}
