package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.ClothesMapper;
import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.services.IClothesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("clothesService")
public class ClothesServiceImpl implements IClothesService {

    @Resource(name = "clothesMapper")
    private ClothesMapper clothesMapper;

    //查询全部数据
    public List<Clothes> getAllClothes(int start,int limit){

        return this.clothesMapper.getAllClothes(start,limit);
    }

    //修改商品信息
    public int editClothes(Clothes clothes){
        return this.clothesMapper.editClothes(clothes);
    }

    //删除选中单行的数据
    public int deleteSelectOne(String clothesId){
        return this.clothesMapper.deleteSelectOne(clothesId);
    }

    //批量删除选中行
    public int batchDelete(String clothesId[]){
        return this.clothesMapper.batchDelete(clothesId);
    }

    //新增商品添加库存
/*    public int addStock(Clothes clothes){
        return this.clothesMapper.addStock(clothes);
    }*/

    //添加商品
    public int addClothes(Clothes clothes){
        return this.clothesMapper.addClothes(clothes);
    }

    //按商品编号搜索
    public List<Clothes> getClothesById(Clothes clothes,int start,int limit){
        return this.clothesMapper.getClothesById(clothes,start,limit);
    }

    //按商品名称搜索
    public List<Clothes> getClothesByName(Clothes clothes,int start,int limit){
        return this.clothesMapper.getClothesByName(clothes,start,limit);
    }

    //按关键字搜索商品
    public List<Clothes> searchClothes(String keyWord,String keyType,int start,int limit){
        return this.clothesMapper.searchClothes(keyWord,keyType,start,limit);
    }

    //获取商品数据总记录数
    public int getClothesCounts(){
        return this.clothesMapper.getClothesCounts();
    }

    //按商品编号获取商品
    public Clothes searchById(String clothesId){
        return this.clothesMapper.searchById(clothesId);
    }

    //根据商品名称查询相同商品名称的商品
    public List<Clothes> selectSameName(String clothesName,String clothesSize){
        return this.clothesMapper.selectSameName(clothesName,clothesSize);
    }


}
