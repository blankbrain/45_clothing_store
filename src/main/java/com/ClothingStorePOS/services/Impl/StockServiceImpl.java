package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.StockMapper;
import com.ClothingStorePOS.pojo.Stock;
import com.ClothingStorePOS.services.IStockService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("stockService")
public class StockServiceImpl implements IStockService {

    @Resource(name = "stockMapper")
    private StockMapper stockMapper;

    public int insertStock(Stock stock){
        return this.stockMapper.insertStock(stock);
    }

    public Stock selectByClothesId(String clothesId){
        return this.stockMapper.selectByClothesId(clothesId);
    }

    public int updateByClothesId(Stock stock){
        return this.stockMapper.updateByClothesId(stock);
    }
}
