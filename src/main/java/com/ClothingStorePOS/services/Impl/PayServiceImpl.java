package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.PayMapper;
import com.ClothingStorePOS.pojo.Pay;
import com.ClothingStorePOS.services.IPayService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("payService")
public class PayServiceImpl implements IPayService {

    @Resource(name = "payMapper")
    private PayMapper payMapper;

    public int insertPay(Pay pay){
        return this.payMapper.insertPay(pay);
    }

}
