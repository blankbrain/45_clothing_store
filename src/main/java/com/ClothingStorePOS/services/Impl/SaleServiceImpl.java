package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.OrderItemMapper;
import com.ClothingStorePOS.dao.OrderMapper;
import com.ClothingStorePOS.pojo.Order;
import com.ClothingStorePOS.pojo.OrderItem;
import com.ClothingStorePOS.services.ISaleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Service("saleService")
public class SaleServiceImpl implements ISaleService {

    @Resource(name = "orderMapper")
    private OrderMapper orderMapper;
    @Resource(name = "orderItemMapper")
    private OrderItemMapper orderItemMapper;

    //添加新的订单
    public int insertNewOrder(Order order) {
        return this.orderMapper.insertNewOrder(order);
    }

    //添加订单记录
    public int insertOrderItem(OrderItem orderItem){
        return this.orderItemMapper.insertOrderItem(orderItem);
    }

    //获取所有挂单订单
    public List<Order> getAllTakeOrder(){
        return this.orderMapper.getAllTakeOrder();

    }

    //根据顾客手机号码获取挂单订单
    public List<Order> selectTakeOrderByPhone(String keyWord){
        return this.orderMapper.selectTakeOrderByPhone(keyWord);

    }

    //删除选定的未处理订单
    public int deletePendingOrder(Long id){
        return this.orderMapper.deletePendingOrder(id);
    }

    //批量删除未处理订单
    public int batchDelPendingOrder(Long[] id){
        return this.orderMapper.batchDelPendingOrder(id);
    }

    //根据订单号查询订单
    public Order selectOrderById(Long id){
        return this.orderMapper.selectOrderById(id);
    }

    //根据订单号删除订单详情表
    public int deleteByOrderId(Long orderId){
        return this.orderItemMapper.deleteByOrderId(orderId);
    }

    public Order selectOrderByOrderId(Long id){
        return this.orderMapper.selectOrderByOrderId(id);
    }

    //根据订单号更新订单
    public int updateOrderByOrderId(Order order){
        return this.orderMapper.updateOrderByOrderId(order);
    }

    //根据订单详情编号更新订单清单
    public int updateOrderItem(OrderItem orderItem){
        return this.orderItemMapper.updateOrderItem(orderItem);
    }

    //根据订单明细id删除订单明细
    public int delByItemId(Long id){
        return this.orderItemMapper.delByItemId(id);
    }

}
