package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.RoleMapper;
import com.ClothingStorePOS.dao.UserRoleMapper;
import com.ClothingStorePOS.services.IRoleService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("roleService")
public class RoleService implements IRoleService {

    @Resource(name = "userRoleMapper")
    private UserRoleMapper userRoleMapper;
    @Resource(name = "roleMapper")
    private RoleMapper roleMapper;

    //根据用户id查找角色id
    public List<Integer> findRoleIdListByUserId(int userId){
        return this.userRoleMapper.findRoleIdListByUserId(userId);
    }

    //按照用户id查找资源权限
    public List<Map<Integer, String>> findRoleResourceListByRoleId(int roleId){
        return this.roleMapper.findRoleResourceListByRoleId(roleId);
    }

    //添加记录
    public int addUserRole(Integer userId, Integer roleId) {
        return this.userRoleMapper.addUserRole(userId,roleId);
    }
}
