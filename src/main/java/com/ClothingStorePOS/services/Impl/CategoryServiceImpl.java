package com.ClothingStorePOS.services.Impl;


import com.ClothingStorePOS.dao.CategoryMapper;
import com.ClothingStorePOS.dao.ClothesMapper;
import com.ClothingStorePOS.pojo.CategoryKey;
import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.services.ICategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("categoryService")
public class CategoryServiceImpl implements ICategoryService {

    @Resource(name = "categoryMapper")
    private CategoryMapper categoryMapper;

    @Resource(name = "clothesMapper")
    private ClothesMapper clothesMapper;

    public List<CategoryKey> getAllCategory(){
        return this.categoryMapper.getAllCategory();
    }

    //获取所有商品数据
    public List<Clothes> getAll(int start, int limit){

        return this.clothesMapper.getAllClothes(start,limit);
    }

    //按类型搜索商品
    public List<Clothes> searchByType(String keyWord,int start,int limit){
        return this.clothesMapper.searchByType(keyWord,start,limit);
    }

    //修改指定类型的衣服
    public int alterChoose(String type,String oldType){
        return this.categoryMapper.alterChoose(type,oldType);
    }

    //新增类别
    public int addCategory(String type){
        return this.categoryMapper.addCategory(type);
    }
}
