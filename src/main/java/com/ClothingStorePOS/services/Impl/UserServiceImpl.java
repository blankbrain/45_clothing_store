package com.ClothingStorePOS.services.Impl;

import com.ClothingStorePOS.dao.UserMapper;
import com.ClothingStorePOS.pojo.User;
import com.ClothingStorePOS.pojo.UserVO;
import com.ClothingStorePOS.services.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements IUserService {
	@Resource(name="userMapper")
	private UserMapper userMapper;

	//登录验证
	public User findUserByLoginName(String username) {
		return this.userMapper.findUserByLoginName(username);
	}

	//添加用户
	public int addUser(User user){
		return this.userMapper.addUser(user);
	}

	//获取所有收银员的信息
	public List<User> findUserByRoleId(int roleId,int start,int limit){
		return this.userMapper.findUserByRoleId(roleId,start,limit);
	}

	//获取收银员的记录数
	public int userCountsSale(int roleId){
		return this.userMapper.userCountsSale(roleId);
	}

	//获取所有用户的信息
	public List<User> findAllUserByRoleId(int roleId1,int roleId2,int start,int limit){
		return this.userMapper.findAllUserByRoleId(roleId1,roleId2,start,limit);
	}

	//获取所有用户的记录数
	public int userCountsSaleAndManager(int roleId1,int roleId2){
		return this.userMapper.userCountsSaleAndManager(roleId1,roleId2);
	}

	//根据id删除用户
	public int deleteByUserId(int id){
		return this.userMapper.deleteByUserId(id);
	}

	//根据用户id查询信息
	public User selectUserById(Integer id){
		return this.userMapper.selectUserById(id);
	}

	public int editUser(UserVO userVO){
		return this.userMapper.editUser(userVO);
	}


}
