package com.ClothingStorePOS.services;

import java.util.List;
import java.util.Map;

public interface IRoleService {

    List<Integer> findRoleIdListByUserId(int userId);
    List<Map<Integer, String>> findRoleResourceListByRoleId(int roleId);

    //user_role表添加记录
    int addUserRole(Integer userId,Integer roleId);
}
