package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.Stock;


public interface IStockService {
    int insertStock(Stock stock);

    Stock selectByClothesId(String clothesId);

    int updateByClothesId(Stock stock);
}
