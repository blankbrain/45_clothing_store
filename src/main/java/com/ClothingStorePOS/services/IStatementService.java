package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.Clothes;
import com.ClothingStorePOS.pojo.OrderItem;

import java.util.Date;
import java.util.List;

public interface IStatementService {
    List<Clothes> searchClothesByCategory(String clothesType);
    List<OrderItem> saleVolumeDateRange(Date startDate, Date endDate, String clothesType);
    List<OrderItem> saleVolumeMonthRange(String clothesId, Date month);
    double turnoverMonthRange(Date startMonth,Date endMonth);
}
