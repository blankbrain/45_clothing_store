package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.Clothes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IClothesService {
    List<Clothes> getAllClothes(int start,int limit);
    int editClothes(Clothes clothes);
    int deleteSelectOne(String clothesId);
    int batchDelete(String clothesId[]);
    int addClothes(Clothes clothes);
    List<Clothes> getClothesById(Clothes clothes, int start,int limit);
    List<Clothes> getClothesByName(Clothes clothes,int start,int limit);
    List<Clothes> searchClothes(String keyWord,String keyType,int start,int limit);
    int getClothesCounts();
    Clothes searchById(String clothesId);
    List<Clothes> selectSameName(String clothesName,String clothesSize);

}
