package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.CategoryKey;
import com.ClothingStorePOS.pojo.Clothes;

import java.util.List;

public interface ICategoryService {
    List<CategoryKey> getAllCategory();
    List<Clothes> getAll(int start, int limit);
    List<Clothes> searchByType(String keyWord,int start,int limit);
    int alterChoose(String type,String oldType);
    int addCategory(String type);
}
