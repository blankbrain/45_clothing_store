package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.Order;
import com.ClothingStorePOS.pojo.OrderItem;

import java.util.ArrayList;
import java.util.List;

public interface ISaleService {
    int insertNewOrder(Order order);
    int insertOrderItem(OrderItem orderItem);
    List<Order> getAllTakeOrder();
    List<Order> selectTakeOrderByPhone(String keyWord);
    int deletePendingOrder(Long id);
    int batchDelPendingOrder(Long[] id);
    Order selectOrderById(Long id);
    int deleteByOrderId(Long orderId);
    Order selectOrderByOrderId(Long id);
    int updateOrderByOrderId(Order order);
    int updateOrderItem(OrderItem orderItem);
    int delByItemId(Long id);
}
