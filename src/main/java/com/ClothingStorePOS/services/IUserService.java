package com.ClothingStorePOS.services;

import com.ClothingStorePOS.pojo.User;
import com.ClothingStorePOS.pojo.UserVO;

import java.util.List;

public interface IUserService {
	User findUserByLoginName(String username);

    //添加用户
	int addUser(User user);

	List<User> findUserByRoleId(int roleId,int start,int limit);

	int userCountsSale(int roleId);

	List<User> findAllUserByRoleId(int roleId1,int roleId2,int start,int limit);

	int userCountsSaleAndManager(int roleId1,int roleId2);

	int deleteByUserId(int id);

	User selectUserById(Integer id);

	int editUser(UserVO userVO);

}
