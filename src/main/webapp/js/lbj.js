var resuestType = "get";
var requestData = "";
var resuestUrl;
var traditional = true; //防止深度序列化

//通用Ajax请求
function ajaxRequest() {
	//使用$.Deferred()和$.when().done(）把异常请求变成同步请求
	//只有这样在同时多个Ajax请求的时候才能保存数据不会窜

	//deferred对象是一个延迟对象，意思是函数延迟到某个点才开始执行，
	//改变执行状态的方法有两个（成功：resolve和失败：reject），
	//分别对应$.when()的两种执行回调（成功回调函数：done和失败回调函数fail）

	var defer = $.Deferred();

	$.ajax({
		traditional: traditional, //防止深度序列化
		type: resuestType,
		dataType: "json",
		url: getRootPath() + resuestUrl,
		data: requestData,
		success: function(resultData) {
			//延迟延时成功后把数据传递给$.when(Deferred).done(function(resultData){})
			defer.resolve(resultData);
		},
		error: function() {
			alert("网络出现异常，请稍后再试！");
		}
	});

	return defer; //返回延迟对象

	//谁调用ajaxRequest()函数，就使用下面这个方式来获取请求成功的数据
	/*
	$.when(ajaxRequest()).done(function (resultData) {
 
		//在这里处理业务
			        
	});
	*/

}

function goLogin() {
	window.location.href = getRootPath() + "/login.jsp"
}

//获取请求基本地址
function getRootPath() {
	// 获取当前网址，如： http://localhost:80/xyd/index.jsp
	var curPath = window.document.location.href;

	// 获取主机地址之后的目录，如： xyd/index.jsp
	var pathName = window.document.location.pathname;
	var pos = curPath.indexOf(pathName);

	// 获取主机地址，如： http://localhost:80
	var localhostPaht = curPath.substring(0, pos);

	// 获取带"/"的项目名，如：/xyd
	var projectName = pathName
		.substring(0, pathName.substr(1).indexOf('/') + 1);

	// return(localhostPaht+projectName);
	return localhostPaht + projectName;
}