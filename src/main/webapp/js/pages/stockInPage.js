var clothesIdflag = false;
var clothesNameflag = false;
var clothesTypeflag = false;
var clothesColorflag = false;
var clothesSizeflag= false;
var tagPriceflag=false;
var discountflag=true;
var clothesMaterialflag=false;
var numflag=true;
var flag = false;
/*$(document).ready(function() {
    $("input[name='sex']").bind("click",function(){
        var obj1 = document.getElementsByName("sex");
        for(var i=0; i<obj1.length; i ++){
            if(obj1[i].checked){
                sex=obj1[i].value;
                sexflag=true;
            }
        }
        if(sexflag){
            $("#sexmsg").text("");
        }else{
            $("#sexmsg").text("请选择性别");
        }
    });
});*/



$(function(){

    $.ajax({//加载所有类别并填充
        <!--提交数据的类型 POST GET-->
        type: "POST",
        <!--提交的网址-->
        url: "/category/find",

        <!--提交的数据-->
        <!--返回数据的格式-->
        dataType: "json",

        <!--成功返回之后调用的函数-->
        success: function (data) {

            console.log(data.data);
            <!--index代表当前循环到第几个索引，item表示遍历后的当前对象-->
            $.each(data.data, function (index, item) {

                console.log(index);
                $("#clothesType").append(new Option(item.type, item.type));// 下拉菜单里添加元素
            });

        }, error: function () {
            alert("查询类别失败！！！")
        }
    });



    $("#clothesId").blur(function(){//查询是否存在该商品并显示信息
        if($("#clothesId").val()==null || $("#clothesId").val()==""){
            $("#clothesIdMsg").text("商品编号不能为空");
        }else{
            $("#clothesIdMsg").text("");
            clothesIdflag = true;
            $.ajax({
                url:"stock/selectClothesOne",
                data:{
                    "clothesId":$("#clothesId").val()
                },
                type:'post',
                dataType:"json",
                success: function(data){
                    if(data!=null){
                        $("#clothesName").val(data.clothesName);
                        $("#clothesType").val(data.clothesType);
                        $("#clothesColor").val(data.clothesColor);
                        $("#clothesSize").val(data.clothesSize);
                        $("#tagPrice").val(data.tagPrice);
                        $("#discount").val(data.discount);
                        $("#discountPrice").val(data.discountPrice);
                        $("#clothesMaterial").val(data.clothesMaterial);

                        //触发select显示出来
                        $("#clothesSize option:selected").focus();
                        $("#clothesType").change();


/*                        $("#clothesType").val(data.clothesType).prop("selected",true);*/
                        $("#clothesName").attr('disabled',true);
                        $("#clothesColor").attr('disabled',true);
                        $("#tagPrice").attr('disabled',true);
                        $("#discount").attr('disabled',true);
                        $("#discountPrice").attr('disabled',true);
                        $("#clothesMaterial").attr('disabled',true);

                        clothesIdflag = true;
                        clothesNameflag = true;
                        clothesTypeflag = true;
                        clothesColorflag = true;
                        clothesSizeflag= true;
                        tagPriceflag=true;
                        discountflag=true;
                        clothesMaterialflag=true;
                        numflag=true;
                        flag = true;
                        layui.form.render('select','sizeRender');

                    }
                }
            });
        }
    });
    $("#num").blur(function(){
        if($("#num").val()==null || $("#num").val()==""){
            $("#numMsg").text("数量不能为空");
            numflag = false;
        }else{
            $("#numMsg").text("");
            numflag = true;
        }
    });

    $("#clothesName").blur(function(){
        if($("#clothesName").val()==null || $("#clothesName").val()==""){
            $("#clothesNameMsg").text("服装名称不能为空");
            clothesNameflag = false;
        }else{
            $("#clothesNameMsg").text("");
            clothesNameflag = true;
        }
    });

    $("#clothesColor").blur(function(){
        if($("#clothesColor").val()==null || $("#clothesColor").val()==""){
            $("#clothesColorMsg").text("颜色不能为空");
            clothesColorflag = false;
        }else{
            var discount = $("#discount").val();
            var tagPrice = $("#tagPrice").val();
            if($("#discount").val()==null || $("#discount").val()==""){
                discount=10;
                $("#discount").val(10);
            }
            var discountPrice = parseFloat(parseInt(discount)*0.1*parseFloat(tagPrice).toFixed(2)).toFixed(2);//计算折后售价
            $("#discountPrice").val(discountPrice);
            $("#discountMsg").text("");
            $("#clothesColorMsg").text("");
            clothesColorflag = true;
        }
    });

    $("#tagPrice").blur(function(){
        if($("#tagPrice").val()==null || $("#tagPrice").val()==""){
            $("#tagPriceMsg").text("吊牌价不能为空");
            tagPriceflag = false;
        }else{
            $("#tagPriceMsg").text("");
            tagPriceflag = true;
        }
    });

/*    $("#discount").blur(function(){
        var discount = $("#discount").val();
        var tagPrice = $("#tagPrice").val();
        if($("#discount").val()==null || $("#discount").val()==""){
            discount=10;
            $("#discount").val(10);
        }
            var discountPrice = parseFloat(parseInt(discount)*0.1*parseFloat(tagPrice).toFixed(2)).toFixed(2);//计算折后售价
            $("#discountPrice").val(discountPrice);
            $("#discountMsg").text("");
            discountflag = true;

    });*/


    $("#clothesMaterial").blur(function(){
/*        alert(clothesIdflag);
        alert(discountflag);
        alert(numflag);*/
        if($("#clothesMaterial").val()==null || $("#clothesMaterial").val()==""){
            $("#clothesMaterialMsg").text("材质不能为空");
            clothesMaterialflag = false;
        }else{
            $("#clothesMaterialMsg").text("");
            clothesMaterialflag = true;
        }
    });


   /* $("#stockInbtn").click(function(){
        if($("#clothesId").val()==null || $("#clothesId").val()==""){
            $("#clothesIdMsg").text("商品编号不能为空");
            clothesIdflag=false;
        }
        if($("#num").val()==null || $("#num").val()==""){
            $("#numMsg").text("入库数量不能为空");
            numflag=false;
        }
        if($("#clothesName").val()==null || $("#clothesName").val()==""){
            $("#clothesNameMsg").text("商品名称不能为空");
            clothesNameflag = false;
        }

        if($("#clothesColor").val()==null || $("#clothesColor").val()==""){
            $("#clothesColorMsg").text("密码不能为空");
            clothesColorflag = false;
        }

        if($("#tagPrice").val()==null || $("#tagPrice").val()==""){
            $("#tagPriceMsg").text("吊牌价不能为空");
            tagPriceflag = false;
        }

/!*        if($("#discount").val()==null || $("#discount").val()==""){
            $("#discountMsg").text("折扣不能为空");
            discountflag = false;
        }*!/

        if($("#clothesMaterial").val()==null || $("#clothesMaterial").val()==""){
            $("#clothesMaterialMsg").text("联系方式不能为空");
            clothesMaterialflag = false;
        }


        if(clothesIdflag==true && clothesNameflag==true && numflag==true && clothesColorflag==true && clothesMaterialflag==true && tagPriceflag==true&&discountflag==true){
            alert("执行了");
            if (flag==false) {//商品不存在
                alert("商品不存在");
                $.ajax({
                    url: "/stock/stockIn",
                    data: {
                        "clothesId": $("#clothesId").val(),
                        "clothesName": $("#clothesName").val(),
                        "clothesType": $('#clothesType option:selected').val(),
                        "clothesColor": $("#clothesColor").val(),
                        "clothesSize": $('#clothesSize option:selected').val(),
                        "tagPrice": $("#tagPrice").val(),
                        "discount": $("#discount").val(),
                        "discountPrice": $("#discountPrice").val(),
                        "clothesMaterial": $("#clothesMaterial").val(),
                        "num": $("#num").val()
                    },
                    type: 'post',
                    dataType: "json",
                    contentType: "application/x-www-form-urlencoded",
                    success: function (data) {
                        if (data.success) {
                            alert("商品入库成功！");
                            window.location.href = "/toStockInPage";
                        } else {
                            alert("商品入库失败！");
                        }
                    }
                });
            }else{//商品已经存在
                $.ajax({
                    url:"/stock/alterStock",
                    data:{
                        "clothesId": $("#clothesId").val(),
                        "stock": $("#num").val()
                    },
                    type:'post',
                    dataType:'json',
                    success:function (data) {
                        if (data.success) {
                            alert("商品入库成功！");
                            window.location.href="/toStockInPage";
                        } else {
                            alert("商品入库失败！");
                        }
                    }
                })

            }
        }
    });*/
});

function stockIn() {
     if($("#clothesId").val()==null || $("#clothesId").val()==""){
         $("#clothesIdMsg").text("商品编号不能为空");
         clothesIdflag=false;
     }
     if($("#num").val()==null || $("#num").val()==""){
         $("#numMsg").text("入库数量不能为空");
         numflag=false;
     }
     if($("#clothesName").val()==null || $("#clothesName").val()==""){
         $("#clothesNameMsg").text("商品名称不能为空");
         clothesNameflag = false;
     }

     if($("#clothesColor").val()==null || $("#clothesColor").val()==""){
         $("#clothesColorMsg").text("颜色不能为空");
         clothesColorflag = false;
     }

     if($("#tagPrice").val()==null || $("#tagPrice").val()==""){
         $("#tagPriceMsg").text("吊牌价不能为空");
         tagPriceflag = false;
     }

/*        if($("#discount").val()==null || $("#discount").val()==""){
         $("#discountMsg").text("折扣不能为空");
         discountflag = false;
     }*/

     if($("#clothesMaterial").val()==null || $("#clothesMaterial").val()==""){
         $("#clothesMaterialMsg").text("联系方式不能为空");
         clothesMaterialflag = false;
     }


     if(clothesIdflag==true && clothesNameflag==true && numflag==true && clothesColorflag==true && clothesMaterialflag==true && tagPriceflag==true&&discountflag==true){
         alert(flag);
         if (flag==false) {//商品不存在
             $.ajax({
                 url: "stock/stockIn",
                 data: {
                     "clothesId": $("#clothesId").val(),
                     "clothesName": $("#clothesName").val(),
                     "clothesType": $('#clothesType option:selected').val(),
                     "clothesColor": $("#clothesColor").val(),
                     "clothesSize": $('#clothesSize option:selected').val(),
                     "tagPrice": $("#tagPrice").val(),
                     "discount": $("#discount").val(),
                     "discountPrice": $("#discountPrice").val(),
                     "clothesMaterial": $("#clothesMaterial").val(),
                     "stock": $("#num").val()
                 },
                 type: 'post',
                 dataType: "json",
                 contentType: "application/x-www-form-urlencoded",
                 success: function (data) {
                     if (data.success) {
                         alert("商品入库成功！");
                         window.location.href = "/toStockInPage";
                     } else {
                         alert("商品入库失败！");
                     }
                 }
             });
         }else{//商品已经存在
             $.ajax({
                 url:"/stock/alterStock",
                 data:{
                     "clothesId": $("#clothesId").val(),
                     "stock": $("#num").val()
                 },
                 type:'post',
                 dataType:'json',
                 success:function (data) {
                     if (data.success) {
                         alert("商品入库成功！");
                         window.location.href="/toStockInPage";
                     } else {
                         alert("商品入库失败！");
                     }
                 }
             })

         }
     }
}


function resetAll() {
    $("#clothesName").attr('disabled',false);
    $("#clothesColor").attr('disabled',false);
    $("#tagPrice").attr('disabled',false);
    $("#discount").attr('disabled',false);
    $("#discountPrice").attr('disabled',false);
    $("#clothesMaterial").attr('disabled',false);
}
/*    $("#rpassword").blur(function(){
        if($("#rpassword").val()==null || $("#rpassword").val()==""){
            $("#rPasswordMsg").text("密码不能为空");
            rpassflag = false;
        }else if($("#password").val()!=$("#rpassword").val()){
            $("#rPasswordMsg").text("两次输入的密码不一致");
            rpassflag = false;
        }else if($("#password").val()==$("#rpassword").val()){
            $("#rPasswordMsg").text("");
            rpassflag = true;
        }
    });*/
/*    $("#email").blur(function(){
        var filter  = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
        if($("#email").val()==null || $("#email").val()==""){
            $("#emailmsg").text("邮箱不能为空");
            emailflag = false;
        }else if(!filter.test($("#email").val())){
            $("#emailmsg").text("邮箱格式不正确");
            emailflag = false;
        }else{
            $("#emailmsg").text("");
            emailflag = true;
        }
    });*/
/*    $("#phone").blur(function(){
        var hm=/^1[3|4|5|7|8][0-9]{9}$/;
        if($("#phone").val()==null || $("#phone").val()==""){
            $("#phonemsg").text("联系方式不能为空");
            uPhoneflag = false;
        }else if(!hm.test($("#phone").val())){
            $("#phonemsg").text("联系方式格式不正确");
            uPhoneflag = false;
        }else{
            $("#phonemsg").text("");
            uPhoneflag = true;
        }
    });*/