layui.use(['form','table','util','element'], function () {
    var form = layui.form
        ,table = layui.table
        ,element = layui.element
        ,util = layui.util;

    /*验证*/
/*    form.verify({
        phone:[/^1[3|4|5|7|8]\d{9}$/, '手机必须11位，只能是数字！']
    })*/



    /*展示搜索到的订单*/
    table.render({
        elem:'#ordersDisplay'
        ,url:'order/findTakeOrder'
        ,title:'订单展示'
        ,id:'pendingOrderReload'
        ,height:450
        ,cols:[[
            {checkbox: true, fixed: true }
            ,{field:'id',title:'订单编号',align:'center'}
            ,{field:'phone', title:'手机号', align:'center'}
            ,{field:'buildTime', title:'生成时间', sort:true,align:'center',
                templet :function (row){
                    return createTime(row.buildTime);
                }}
            ,{field:'cashierid', title:'销售人员', align:'center'}
            ,{field:'amount', title:'总金额',align:'center'}
            ,{field: 'barDemo', align:'center', title: '操作', toolbar: '#barDemo'}
        ]]
    });

    /**
     * 搜索
     */
/*    var $ = layui.$, active = {

        reload: function () {

            //获取select选中的值
            var keyWord = $("#telphone").val();
            console.log(keyWord);
            table.reload('pendingOrderReload', {

                 where: {

                    keyWord:keyWord,

                }
            });
        }
    }*/
    form.on('submit(search)', function() {

        var keyWord = $("#telphone").val();
        console.log("手机尾号为："+keyWord);
        //执行重载
        table.reload('pendingOrderReload', {
            where: {
                keyWord: keyWord
            }
        })
        return false;

    });

    /**
     * 监听工具条
     */
    table.on('tool(test)', function(obj){
        //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            , layEvent = obj.event; //获得 lay-event 对应的值
        if(layEvent === 'detail'){

        }else if(layEvent === 'del'){
            layer.alert('将要删除该订单,确定吗?', {
                closeBtn: 0    // 是否显示关闭按钮
                , anim: 6
                , btn: ['确定', '取消'] //按钮
                , yes: function (index) {
                    // obj.del();
                    // layer.close(index);
                    var getData = new Object();
                    getData.id = data.id;
                    $.ajax({
                        url: '/order/deletePendingOrder',
                        type: 'post',
                        data: getData,
                        dataType: 'json',
                        success: function () {
                            location.reload(true);
                        },
                        error: function () {
                            alert("删除错误,请重新编写!");
                        }
                    });
                    layer.close(index);
                }
            });
        } else if(layEvent === 'take'){
                layer.alert('确定取单?', {
                    closeBtn: 0    // 是否显示关闭按钮
                    , anim: 6
                    , btn: ['确定', '取消'] //按钮
                    , yes: function (index) {
                        // obj.del();
                        // layer.close(index);
                        var getData = new Object();
                        getData.orderId = data.id;
                        $.ajax({
                            url: '/order/takePendingOrder',
                            type: 'post',
                            data: getData,
                            dataType: 'json',
                            success: function (data) {
                                if (data.success)
                                    window.location.href="/toSalePage?flag=true";
/*                                $("#makeNewSale").attr('disabled',true);
                                $("#dec").attr('disabled',false);
                                $("#inc").attr('disabled',false);
                                $("#confirm").attr('disabled',false);
                                $("#pendingOrder").attr('disabled',false);
                                $("#chargeMoney").attr('disabled',false);
                                $("#cancelOrder").attr('disabled',true);*/
                            },
                            error: function () {
                                alert("取单失败!");
                            }
                        });
                        layer.close(index);
                    }
                });
            }
    });
/*    $('#search').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });*/
});


/**
 * 批量删除
 */
function batchDelPendingOrder() {
    var checkStatus = layui.table.checkStatus('pendingOrderReload')
        ,data = checkStatus.data;
    var str = "" ;
    if(data.length>0){
        for(var i=0; i<data.length; i++){
            str+=data[i].id+",";
        }
        layer.confirm('是否删除这'+data.length+'个未处理订单',{icon:3,title:'提示'},function (index) {
            $.ajax({
                url:'/order/batchDelPendingOrder',
                type:'post',
                data:{
                    'str' : str
                },
                dataType:'json',
                success:function(){
                    alert("删除成功！")
                    location.reload(true);
                },
                error:function () {
                    alert("删除错误,请重新操作!");
                }
            });
            layer.close(index);
        });
    }else{
        layer.alert("请选择要删除的数据!")
    }
}

/**
 * 查询全部
 */
function getAll() {
    layui.table.render({
        elem:'#ordersDisplay'
        ,url:'order/findTakeOrder'
        ,title:'订单展示'
        ,id:'pendingOrder'
        ,height:450
        ,cols:[[
            {checkbox: true, fixed: true }
            ,{field:'id',title:'订单编号',align:'center'}
            ,{field:'phone', title:'手机号', align:'center'}
            ,{field:'buildTime', title:'生成时间',sort:true,align:'center',
                templet :function (row){
                    return createTime(row.buildTime);
                }}
            ,{field:'cashierid', title:'销售人员',  align:'center'}
            ,{field:'amount', title:'总金额', align:'center'}
            ,{field: 'barDemo', align:'center', title: '操作', toolbar: '#barDemo'}
        ]]
    });

}

/**
 * layui表格的创建时间字段转化成时间格式
 * @param v
 * @returns {string}
 */
function createTime(v){
    var date = new Date(v);
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    m = m<10?'0'+m:m;
    var d = date.getDate();
    d = d<10?("0"+d):d;
    var h = date.getHours();
    h = h<10?("0"+h):h;
    var M = date.getMinutes();
    M = M<10?("0"+M):M;
    var str = y+"-"+m+"-"+d+" "+h+":"+M;
    return str;
}

