layui.use(['layer', 'upload', 'form','table'], function() {
    var layer = layui.layer,
        form = layui.form,
        table = layui.table;

})

function chargeMoney(){

    layer.open({
        type:2
        , title: '支付页面'
        , area: ['550px','600px']
        ,btn: ['结算', '取消']
        , content: "/sale/toPayPage"
/*        ,success: function(layero, index){
            layui.use( 'form', function() {
                var form = layui.form;
                    form.render()
            });*/
/*            var ars = parent.layui.table.cache.buyItems;
            console.log(ars);*/
            /*将父页面表格数据传给后台，计算总金额*/
          /*  $.ajax({
               url: '/sale/chargeMoney',
               type: 'post',
               dataType: 'json',
               contentType: "application/x-www-form-urlencoded",
               data: {},
               success: function (data) {
                   $('#countMoney').val(date);
               },
               error: function () {
                   alert("计算错误!");
               }
           });
          */
        ,yes: function(index, layero) {
            /*计算找零*/
            var ibody = layer.getChildFrame('body', index);//获取iframe页面body
            var realMoney = ibody.find("#realMoney").val();
            var countMoney = ibody.find("#countMoney").val();
            var postData = new Object();
            if (ibody.find("#bankPay").is(":checked") || ibody.find("#weChatPay").is(":checked") || ibody.find("#aliPay").is(":checked")) {

                if (ibody.find("#bankPay").is(":checked")) {
                    alert("银联被选中" + realMoney);
                    postData.payStyle = ibody.find("#bankPay").val();
                } else if (ibody.find("#weChatPay").is(":checked")) {
                    alert("微信支付被选中" + realMoney);
                    postData.payStyle = ibody.find("#weChatPay").val();
                } else if (ibody.find("#aliPay").is(":checked")) {
                    postData.payStyle = ibody.find("#aliPay").val();
                }
            } else {
                if (realMoney == null)
                    alert("请输入付款金额");
                else if (realMoney!=null) {
                    if (realMoney >= countMoney) {
                        var change = parseFloat(parseFloat(realMoney).toFixed(2)-parseFloat(countMoney).toFixed(2)).toFixed(1);
                        ibody.find("#makeChange").val(change);
                        alert("实收金额为："+realMoney+"\n"+"找零："+change);
                        postData.payStyle = 1;
                    } else
                        alert("实收金额不足以支付该笔订单！");
/*                        var change = parseFloat($('#realMoney').val()).toFixed(2) - parseFloat($('#countMoney').val()).toFixed(2);
                    $('#makeChange').val(change);*/

                }
            }
            if (postData.payStyle!=null) {
                $.ajax({
                    url: '/sale/chargeMoney',
                    type: 'post',
                    data: postData,
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded",
                    success: function (data) {
                        if (data.success) {
                            alert("支付成功！");
                            location.reload(true);
                            layer.close(index);
                            $("#makeNewSale").attr('disabled',false);
                            $("#dec").attr('disabled',true);
                            $("#inc").attr('disabled',true);
                            $("#confirm").attr('disabled',true);
                            $("#pendingOrder").attr('disabled',true);
                            $("#chargeMoney").attr('disabled',true);
                            $("#cancelOrder").attr('disabled',true);
                        }
                    },
                    error: function () {
                        alert("编辑错误,请重新编写!");
                        layer.close(index);
                    }
                });
            }
        }
       , cancel: function(index, layero){
        if(confirm('还未支付！确定要取消支付吗')){ //只有当点击confirm框的确定时，该层才会关闭
            layer.close(index);
        }
        return false;
    }
})
    /**动态渲染单选框**/
    from.render('radio');
}

function chargeOrder(){

}
