$(function () {

    <!--数量增减组件-->
    $('#dec').click(function() {
        if( $('#count').val()>1)
            $('#count').val(parseInt(count.value) - 1);
    });
    $('#inc').click(function() {
        $('#count').val(parseInt(count.value) + 1);
    });

    $.ajax({//加载所有类别并填充
        <!--提交数据的类型 POST GET-->
        type: "POST",
        <!--提交的网址-->
        url: "/category/find",

        <!--提交的数据-->
        <!--返回数据的格式-->
        dataType: "json",

        <!--成功返回之后调用的函数-->
        success: function (data) {

            console.log(data.data);
            <!--index代表当前循环到第几个索引，item表示遍历后的当前对象-->
            $.each(data.data, function (index, item) {

                console.log(index);
                $("#clothesTypeSel").append(new Option(item.type, item.type));// 下拉菜单里添加元素
            });

        }, error: function () {
            alert("查询类别失败！！！")
        }
    });

    <!--填充到类型和尺码select-->
    var clothesType = $("#clothesType").val();
    var clothesSize = $("#clothesSize").val();
    $("#clothesTypeSel").val(clothesType);
    $("#clothesSizeSel").val(clothesSize);
    $("#clothesTypeSel").change();
    $("#clothesSizeSel").change();

    <!--计算折后价格-->
    $("#discount").blur(function(){
        var discount = $("#discount").val();
        var tagPrice = $("#tagPrice").val();
        if($("#discount").val()==null || $("#discount").val()==""){
            discount=10;
        }
            var discountPrice = parseFloat(parseInt(discount)*0.1*parseFloat(tagPrice).toFixed(2)).toFixed(2);//计算折后售价
            $("#discountPrice").val(discountPrice);

    });

});