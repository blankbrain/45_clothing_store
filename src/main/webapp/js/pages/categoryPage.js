$(document).ready(function() {
    layui.use(['table', 'form', 'layer', 'layedit'], function () {

        $ = layui.jquery;
        var form = layui.form;
        var table = layui.table;
        var layer = layui.layer;
        var layedit = layui.layedit;

        <!--加载表格-->
        table.render({
            elem: '#categoryTable'
            , url: '/category/getAll'
            , id: 'categoryReload'
            ,height:450
            , cols: [[//表头
                {field: 'clothesId', title: '商品编号', sort: true, align: 'center'}
                , {field: 'clothesName', title: '商品名称',  align: 'center'}
                , {field: 'clothesType', title: '类型',  align: 'center'}
                , {field: 'clothesColor', title: '颜色',  align: 'center'}
                , {field: 'clothesSize', title: '尺码', align: 'center'}
                , {field: 'stock', title: '库存',  sort: true, align: 'center'}
                , {field: 'tagPrice', title: '吊牌价', align: 'center'}
                , {field: 'discount', title: '折扣',  align: 'center'}
                , {field: 'discountPrice', title: '折后售价',  align: 'center'}
            ]]
            , count: 20
            , limit: 10
            , page: true//开启分页
        });

        <!--页面打开时异步加载数据-->
        $(function () {
            $.ajax({
                <!--提交数据的类型 POST GET-->
                type: "POST",
                <!--提交的网址-->
                url: "/category/find",

                <!--提交的数据-->
                <!--返回数据的格式-->
                dataType: "json",

                <!--成功返回之后调用的函数-->
                success: function (data) {

                    console.log(data.data);
                    <!--index代表当前循环到第几个索引，item表示遍历后的当前对象-->
                    $.each(data.data, function (index, item) {

                        console.log(index);
                        $('#categoryItem').append(new Option(item.type, index+1));// 下拉菜单里添加元素
                    });

                    form.render('select', 'lf');
                }, error: function () {
                    alert("查询类别失败！！！")
                }
            })
        });

        //select(lay-filter的值)
/*        form.on('select', function (data) {//获取select选中的值

            //获取select选中的值
            var keyWord = data.value;
            console.log(keyWord);
            //执行重载
            table.reload('categoryReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {

                    keyWord: keyWord

                }
            });
        });*/


        var $ = layui.$, active = {

            //搜索指定类别的所有商品
            reload: function () {

                //获取select选中的option的text
                var item = document.getElementById("categoryItem");
                var keyWord  =  item.options[item.selectedIndex].text;

                //执行重载
                table.reload('categoryReload', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , where: {

                        keyWord: keyWord

                    }
                });
            },
            //修改指定类别
            alter:function () {

                var getData = new Object();
                var item = document.getElementById("categoryItem");
                getData.tid = $("#categoryItem").val();
                getData.oldType =  item.options[item.selectedIndex].text;
                /*$("select option:selected").text();*/
                /*$("#categoryItem").options[$("#categoryItem").selectedIndex].text();*/


                if (getData.oldType!=null||getData.oldType!=""){
                    layer.open({
                        type: 1
                        , title: '更改类别'
                        , area: '350px'
                        , closeBtn: false
                        , btn: ['确认更改', '取消']
                        , btnAlign: 'c'
                        , content: '<form class="layui-form" id="alterChoose">' +
                            '        <div class="layui-form-item"  style="padding: 20px; line-height: 24px;">' +
                            '            <label class="layui-form-label">类别名:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="hidden" required lay-verify="required" name="tid"  value="' + getData.tid + '"  class="layui-input">' +
                            '            </div>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="type"  id="type" value="' + getData.oldType + '"  class="layui-input">' +
                            '            </div>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="hidden" required lay-verify="required" name="oldType"  value="' + getData.oldType + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '    </form>'
                        , yes: function (index) {
                            $.ajax({
                                url: '/category/alterChoose',
                                type: 'post',
                                data: $("#alterChoose").serialize(),
                                dataType: 'json',
                                contentType: "application/x-www-form-urlencoded",
                                success: function (data) {
                                    location.reload(true);
                                },
                                error: function () {
                                    alert("该类型已存在,请重新编写!");
                                }
                            });
                            layer.close(index);
                        }
                    });
                } else{
                        alert("请先选择修改的类型！")
                    }

            }


            };


            $('#sousuo').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
            });
            $('#alterType').on('click', function(){
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });



    });

    function add(){
        layer.open({
            type: 1
            , title: '新增服装类别'
            , area: '350px'
            , closeBtn: false
            , content: '<form class="layui-form" id="add">' +
                '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                '            <label class="layui-form-label">服装类别:</label>' +
                '            <div class="layui-input-inline">' +
                '                <input type="text" required lay-verify="required" name="type" placeholder="请输入商品编号"   class="layui-input">' +
                '            </div>' +
                '        </div>' +
                '    </form>'
            , btn: ['确认添加', '取消']
            , btnAlign: 'c'
            , yes: function (index) {
                $.ajax({
                    url: '/category/add',
                    type: 'post',
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded",
                    data: $("#add").serialize(),
                    success: function (data) {
                        location.reload(true);
                    },
                    error: function () {
                        alert("该类型已存在,请重新编写!");
                    }
                });
                layer.close(index);
            }
        });
    }

})



/*layer.alert('将要删除该类型的所有衣服,确定要删除吗?', {
    closeBtn: 0    // 是否显示关闭按钮
    , anim: 6
    , btn: ['确定', '取消'] //按钮
    , yes: function (index) {
        // obj.del();
        // layer.close(index);
        //获取select选中的值
        var getData = new Object();
        getData.type = $("#categoryItem").val();
        $.ajax({
            url: '/category/alterChoose',
            type: 'post',
            data: getData,
            dataType: 'json',
            success: function () {
                location.reload(true);
            },
            error: function () {
                alert("删除错误,请重新删除!");
            }
        });
        layer.close(index);
    }
});*/
