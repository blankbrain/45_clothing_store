//表单验证事件
function addUserForm() {

	//给表单绑定JQuery的validate验证功能
    return $("#addUserForm").validate({

		//修改错误显示错误信息显示的标签为div，默认的是lable
		//errorElement:"div",

		//修改验证通过时标签的样式
		validClass: "ok",

		rules: {
			//标签的name属性值{设置规则项}
			loginname: {
				required: true, //这个意思是必填项
				//字符串长度
				rangelength: [1, 12],

				//验证是否已经存在
                remote: {
                    type:"POST",
                    url:"/user/findByLoginName",
                    data:{
                        loginname: function() {return $("#loginname").val();}

                    }
                    }


			},
			'password': {
				required: true,
				equalTo: "#password2",
				rangelength: [5, 18],
				//regex: "[0-9]"//正则
			},
			'password2': {
				required: true,
				equalTo: "#password",
				rangelength: [5, 18],
				//regex: "[0-9]"//正则
			},
			'phone':{
                required: true,
                rangelength:22,
			}
		},

		//修改错误标签出现的地方
		errorPlacement: function(error, element) {
			//取得验证不通过的标签的name
			var elName = $(element).attr("name");

			$(element).parent().append(error);
			//把错误信息添加到验证标签之后，作为兄弟标签
			//$(error).insertAfter(element);
			//$(element).css("color", "red");
			$(error).css("color", "red");
			$(error).css("text-align", "left");
		},

		//把错误的标签显示边框颜色
		highlight: function(element, errorClass, validClass) {
			$(element).css("border-color", "red");
		},

		//把正确的标签显示边框颜色恢复
		success: function(label, element) {
			$(element).css("border-color", "");

			//当标签通过验证的时候就把label标签的错误样式删除，然后添加ok样式
			//$(label).remove("error").addClass("ok");
			$(label).addClass("ok");
		},

		//清除ok样式
		unhighlight: function(element, errorClass) {
			$(element).removeClass("ok");
		},

		//自定义错误提示信息
		messages: {
			loginname: {
				required: "用户名不能为空！",
				//{}是占位符
				rangelength: "请输入{0}到{1}长度的用户名！",
                remote: "用户名已存在！"
			},
			password: {
				required: "密码不能为空！",
				rangelength: "请输入{0}到{1}长度的密码！",
				equalTo: "两次密码输入不一致",
				//regex: "密码必须包含大小写字母和数字的组合，不能使用特殊字符"
			},
			password2: {
				required: "密码不能为空！",
				rangelength: "请输入{0}到{1}长度的密码！",
				equalTo: "两次密码输入不一致"
				//regex: "密码必须包含大小写字母和数字的组合，不能使用特殊字符"
			},
			phone:{
                required: "手机号码不能为空！",
                rangelength: "请输入长度为{0}的手机号码！",
			}
		},


	});

	//document.getElementById("addTravel_form").action = "/addTravel_form.action";
}


//清空表单
/*    function clearUserForm() {
        $(':input', '#addUserForm')
            .not(':button, :submit, :reset, :hidden,:radio') // 去除不需要重置的input类型
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    }*/
