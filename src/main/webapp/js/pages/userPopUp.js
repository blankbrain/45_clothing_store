layui.use(['table', 'element', 'layer'], function () {
    var $ = layui.jquery
        , layer = layui.layer        //弹层
        , table = layui.table      //表格
    table.render();
    //监听工具条
    table.on('tool(demo)', function (obj) {
        //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            , layEvent = obj.event; //获得 lay-event 对应的值
        if (layEvent === 'detail') {
            layer.open({
                type: 1
                , title: '用户信息查询'
                , area: '450px'
                , closeBtn: false
                , btn: '确定'
                , btnAlign: 'c'
                , content: '<form class="layui-form layui-form-pane" style="margin-left: 50px">' +
                    '<div style="line-height: 24px;">' +
                    '        <div class="layui-form-item" style="margin-top: 10px;">' +
                    '            <label class="layui-form-label">账号/工号</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="clothesId" required  lay-verify="required" class="layui-input" readonly="readonly" value="' + data.loginname + '">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-form-item">' +
                    '            <label class="layui-form-label">姓名</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="clothesName" required lay-verify="required" class="layui-input" readonly="readonly" value="' + data.name + '">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-form-item">' +
                    '            <label class="layui-form-label">身份</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="clothesType" required lay-verify="required" class="layui-input" readonly="readonly" value="' + data.roleName + '">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-form-item">' +
                    '            <label class="layui-form-label">性别</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="clothesColor" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.sex + '">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-form-item">' +
                    '            <label class="layui-form-label">年龄</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="clothesSize" required lay-verify="required" oninput="value=value.replace(/[^\\d]/g,\'\')" class="layui-input" readonly="readonly" value="' + data.age + '">' +
                    '            </div>' +
                    '        </div>' +
/*                    '        <div class="layui-form-item">' +
                    '            <label class="layui-form-label">创建日期</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" name="stock" required lay-verify="required" class=" layui-input" readonly="readonly" value="' + data.createdate + '">' +
                    '            </div>' +
                    '        </div>' +*/
                    '    </div>'+
                    '</form>'
            });
        } else if (layEvent === 'del') {
            layer.alert('将要删除该行数据,确定吗?', {
                closeBtn: 0    // 是否显示关闭按钮
                , anim: 6
                , btn: ['确定', '取消'] //按钮
                , yes: function (index) {
                    // obj.del();
                    // layer.close(index);
                    var getData = new Object();
                    getData.id = data.id;
                    $.ajax({
                        url: '/user/delete',
                        type: 'post',
                        data: getData,
                        dataType: 'json',
                        success: function () {
                            location.reload(true);
                        },
                        error: function () {
                            alert("删除错误,请重新编写!");
                        }
                    });
                    layer.close(index);
                }
            });
        } else if (layEvent === 'edit') {
            var id = data.id;
/*            userVo.id = data.id;
            userVo.loginname = data.loginname;
            userVo.name = data.name;
            userVo.sex = data.sex;
            userVo.age = data.age;
            userVo.createdate = data.createdate;
            userVo.phone = data.phone;*/
            layer.open({
                type: 2
                , title: '修改用户信息'
                , area: ['600px', '500px']
                , closeBtn: false
                , btn: ['确认更改', '取消']
                , btnAlign: 'c'
                , content: '/user/toAlterUserPage?id='+id
                /*'<form class="layui-form layui-form-pane" id="edit">' +*/
/*                    '        <div class="layui-form-item"  style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">账号/工号:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="clothesId" readonly="readonly"  value="' + data.loginname + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">姓名:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text"  required lay-verify="required" name="clothesName" placeholder="请输入姓名" value="' + data.name + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">身份:</label>' +
                    '            <div class="layui-input-inline">' +
                    '<select name="keyRole" id="key_role">'+
                    '<option value="'+data.roleId+'">'+data.roleName+'</option>'+
                    '<option></option>'+
                    '</select>'+
                    /!*'                <input type="text" required   lay-verify="required" name="clothesType" placeholder="请输入身份" value="' + data.roleName + '"  class="layui-input">' +*!/
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px" >' +
                    '            <label class="layui-form-label">性别:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required   lay-verify="required" name="clothesColor" readonly="readonly" value="' + data.sex + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">联系方式:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="phone" name="clothesSize" placeholder="请输入联系方式" value="' + data.phone + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '    </form>'*/
                , yes: function (index) {
                    var ibody = layer.getChildFrame('body', index);//获取iframe页面body
                    var form = ibody.find('#alterUserForm');
                    ibody.find("#roleName").val(ibody.find("#roleId").find("option:selected").text());

                    /*var postData = new Object();*/
                    var postData = form.serialize();

/*                    postData.roleId = ibody.find("#userRole").val();*/
                    /*postData.roleName = ibody.find("#roleName").find("option:selected").text();*/

                    console.log(postData);
/*                    alert("获取的对象为"+postData+"roleName为"+ibody.find("#roleName").val());
                    alert(postData.id);*/
                    $.ajax({
                        url: '/user/editUser',
                        type: 'post',
                        data: postData,
                        dataType: 'json',
                        contentType: "application/x-www-form-urlencoded",
                        success: function () {
                            location.reload(true);
                            alert("修改成功！");
                        },
                        error: function () {
                            alert("编辑错误,请重新编写!");
                        }
                    });
                    layer.close(index);
                }
            });

/*            if (data.roleId===1){
                console.log("roleId为"+ data.roleId);
                document.getElementById("#key_role").prepend("<option value='1'>经理</option>");
                document.getElementById("#key_role").append("<option value='2'>收银员</option>");
            }else if(data.roleId===2){
                document.getElementById("#key_role").prepend("<option value='2'>收银员</option>");
                document.getElementById("#key_role").append("<option value='1'>经理</option>");
            }*/
        }
    });
});
