layui.use('laydate', function(){
    var laydate = layui.laydate;

    //执行一个laydate实例
    laydate.render({
        elem: '#dateRange'
        ,range: true//指定日期范围
        ,type:'date'
        ,done:function(value, date, endDate){//渲染饼图数据
            console.log(value); //得到日期生成的值，如：2017-08-18
            console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            var getData = new Object();
            //获取select选中的option的text
            var item = document.getElementById("clothClass");
            var clothesType  =  item.options[item.selectedIndex].text;
            getData={
                dateRange:value,
                clothesType:clothesType
            };
            $.ajax({
                url: '/saleStatement/saleVolumeDateRange',
                type: 'post',
                data: getData,
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded",
                success: function (data) {
                    //将返回的数据填充到chart1.series上
                    browsers = [],
                        //迭代，把异步获取的数据放到数组中
                        $.each(data,function(i,d){
                            browsers.push([i,d]);
                        });
                    //设置数据
                    chart1.series[0].setData(browsers);
                },
                error: function () {
                    /*alert("数据获取失败！");*/
                }
            });
        }
    });
/*    laydate.render({
        elem: '#endDate' //指定结束日期
    });*/
    laydate.render({
        elem: '#saleYear1' //指定销量查询年份
        ,type: 'year'
        ,done:function(value){//渲染折线图数据
            console.log(value); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            var getData = new Object();
            //获取select选中的option的值（clothesId）
            var clothesId  =  $("#clothName").val();
            getData={
                year:value,
                clothesId:clothesId
            }
            $.ajax({
                url: '/saleStatement/saleVolumeMonthRange',
                type: 'post',
                data: getData,
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded",
                success: function (data) {
/*                    //将返回的数据填充到chart1.series上
                    browsers = [],
                        //迭代，把异步获取的数据放到数组中
                        $.each(data,function(i,d){
                            browsers.push([i,d]);
                        });*/
                    //设置数据
                    chart2.series[0].setData(data);
                },
                error: function () {
                    /*alert("数据获取失败！");*/
                }
            });
        }
    });
    laydate.render({
        elem: '#saleYear2' //指定营业额查询年份
        ,type: 'year'
        ,done:function(value){//渲染折线图数据
            console.log(value); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            var getData = new Object();
            getData.year=value;
            $.ajax({
                url: '/saleStatement/turnoverMonthRange',
                type: 'post',
                data: getData,
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded",
                success: function (data) {
                    //设置数据
                    chart3.series[0].setData(data);
                },
                error: function () {
                    /*alert("数据获取失败！");*/
                }
            });
        }
    });
});

/*渲染服装类别选择框*/
$(function(){
    $.ajax({
        url: '/saleStatement/getCategory',
        type: 'post',
 /*       data: getData,*/
        dataType: 'json',
/*        contentType: "application/x-www-form-urlencoded",*/
        success: function (data) {
/*            for(var i=1;i<date.length;i++){
                text = "<option value="+"\'"+date[i].value+"\'"+">"+date[i].value+"</option>";
                $('#clothClass').append(text);
            }*/
            <!--index代表当前循环到第几个索引，item表示遍历后的当前对象-->
            $.each(data.data, function (index, item) {
                $('#clothClass').append(new Option(item.type, index+1));
                $('#clothType').append(new Option(item.type, index+1));// 下拉菜单里添加元素
            });
            /*form.render('select', 'lf');*/
        },
        error: function () {
            /*alert("数据读取失败！");*/
        }
    });
});
/**
 * 监听服装类别选择框
 */
$("#clothType").change(function(){
    //要触发的事件
    var clothType = $("#clothType").find("option:selected").text();
    var getData = new Object();
    getData.clothesType = clothType;
    $.ajax({
        url: '/saleStatement/getClothesByCategory',
        type: 'post',
        data: getData,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded",
        success: function (data) {
            $('#clothName').empty();
            $.each(data, function (index, item) {
                $('#clothName').append(new Option(item.clothesName, item.clothesId));// 下拉菜单里添加元素
            });
        },
        error: function () {
           /* alert("数据获取失败！");*/
        }
    });

});

/*同一类别不同服装销售量饼图*/
chart1 = Highcharts.chart('saleChart1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
    },
    title: {
        text: '同一类别各服装销售量'
    },
    credits:{
        enabled:false,
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>件'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} 件',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: '服装销售量',
        colorByPoint: true,
        data: [{
            name: '',
            y: 0,
        }]
    }]
});

/*某服装指定年份的月销售量*/
chart2 = Highcharts.chart('saleChart2', {
    title: {
        text: '服装月销售量'
    },
    yAxis: {
        title: {
            text: '销售量：件'
        }
    },
    xAxis: {
        categories: [
            '一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'
        ],
        crosshair: true
    },
    credits:{
        enabled:false,
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    tooltip: {
        // head + 每个 point + footer 拼接成完整的 table
        headerFormat: '<span style="font-size:10px">{point.key}</span>',
        pointFormat: '<span style="font-size:10px">：<b>{point.y}</b> 件</span>',
        shared: true,
        useHTML: true
    },
    series: [{
        name: '服装销售量/件',
        data: [0, 0, 0, 0, 0, 0, 0, 0,0,0,0]
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});


/*季度销售额展示*/
chart3 = Highcharts.chart('saleChart3',{
    chart: {
        type: 'column',
    },
    title: {
        text: '季度营业额'
    },
    credits:{
        enabled:false,
    },
    xAxis: {
        categories: [
            '第一季度','第二季度','第三季度','第四季度'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '营业额：元'
        }
    },
    tooltip: {
        // head + 每个 point + footer 拼接成完整的 table
        headerFormat: '<span style="font-size:10px">{point.key}</span>',
        pointFormat: '<span style="font-size:10px">营业额:<b>{point.y:.1f} </b>元</span>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            borderWidth: 0
        }
    },
    series: [{
        name:'服装营业额/元',
        data: [0, 0, 0, 0]
    }]
});
//渲染柱形图数据
/*function chart3Data () {
    var getData = new Object();
    getData={
        year:$('#saleYear2').val()
    };
    $.ajax({
        url: '/获取营业额',
        type: 'post',
        data: getData,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded",
        success: function (data) {
            //将返回的数据填充到chart3.series上

        },
        error: function () {
            alert("数据获取失败！");
        }
    });*/
