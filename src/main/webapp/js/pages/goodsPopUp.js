    // 实例化表单,让表单上面的数据显示
    //数据修改弹窗
    // 自定义类型：参数为数组，可多条数据

/*    alignmentFns.createType([{"test": {"step" : 10, "min" : 10, "max" : 999, "digit" : 0}}]);

    // 初始化
    alignmentFns.initialize();

    // 销毁
    alignmentFns.destroy();

    // js动态改变数据
    $("#4").attr("data-max", "12")
    // 初始化
    alignmentFns.initialize();*/

        layui.use(['table', 'element', 'layer'], function () {
            var $ = layui.jquery
                , layer = layui.layer        //弹层
                , table = layui.table      //表格
            table.render();
            //监听工具条
            table.on('tool(demo)', function (obj) {
                //注：tool是工具条事件名，demo是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data //获得当前行数据
                    , layEvent = obj.event; //获得 lay-event 对应的值
                if (layEvent === 'detail') {
                    layer.open({
                        type: 1
                        , title: '服装信息查询'
                        , area: '450px'
                        , closeBtn: false
                        , btn: '确定'
                        , btnAlign: 'c'
                        , content: '<form class="layui-form layui-form-pane" style="margin-left: 50px">' +
                            '<div style="line-height: 24px;">' +
                            '        <div class="layui-form-item" style="margin-top: 10px;">' +
                            '            <label class="layui-form-label">商品编号</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesId" required  lay-verify="required" class="layui-input" readonly="readonly" value="' + data.clothesId + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">商品名称</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesName" required lay-verify="required" class="layui-input" readonly="readonly" value="' + data.clothesName + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">服装类型</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesType" required lay-verify="required" class="layui-input" readonly="readonly" value="' + data.clothesType + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">颜色</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesColor" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.clothesColor + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">尺码</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesSize" required lay-verify="required" class="layui-input" readonly="readonly" value="' + data.clothesSize + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">库存</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="stock" required lay-verify="required" class=" layui-input" readonly="readonly" value="' + data.stock + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">吊牌价</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="tagPrice" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.tagPrice + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">折扣</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="discount" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.discount + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">折后价</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="discountPrice" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.discountPrice + '">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">材质</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" name="clothesMaterial" required lay-verify="required"class="layui-input" readonly="readonly" value="' + data.clothesMaterial + '">' +
                            '            </div>' +
                            '        </div>' +
                            '    </div>'+
                        '</form>'
                    });
                } else if (layEvent === 'del') {
                    layer.alert('将要删除该行数据,确定吗?', {
                        closeBtn: 0    // 是否显示关闭按钮
                        , anim: 6
                        , btn: ['确定', '取消'] //按钮
                        , yes: function (index) {
                            // obj.del();
                            // layer.close(index);
                            var getData = new Object();
                            getData.clothesId = data.clothesId;
                            $.ajax({
                                url: '/clothes/delete',
                                type: 'post',
                                data: getData,
                                dataType: 'json',
                                success: function () {
                                    location.reload(true);
                                },
                                error: function () {
                                    alert("删除错误,请重新编写!");
                                }
                            });
                            layer.close(index);
                        }
                    });
                } else if (layEvent === 'edit') {
                    var clothesId = data.clothesId;
                    layer.open({
                        type: 2
                        , title: '更改服装信息'
                        , area: ['500px', '700px']
                        , closeBtn: false
                        , btn: ['确认更改', '取消']
                        , btnAlign: 'c'
                        ,content:'/clothes/toAlterClothesPage?clothesId='+clothesId
            /*            , content: '<form class="layui-form layui-form-pane" id="edit">' +
                            '        <div class="layui-form-item"  style="padding-left: 20px; padding-top: 10px; line-height: 24px;">' +
                            '            <label class="layui-form-label">商品编号:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="clothesId" readonly="readonly"  value="' + data.clothesId + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label" style="padding-left: 20px;">商品名称:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text"  required lay-verify="required" name="clothesName" placeholder="请输入商品名称" value="' + data.clothesName + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">类型:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required   lay-verify="required" name="clothesType" placeholder="请输入类型" value="' + data.clothesType + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item"  >' +
                            '            <label class="layui-form-label">颜色:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required   lay-verify="required" name="clothesColor" placeholder="请输入颜色" value="' + data.clothesColor + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">尺码:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="clothesSize" placeholder="请输入尺码" value="' + data.clothesSize + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item" >' +
                            '            <label class="layui-form-label">库存:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text"  required lay-verify="required" name="stock" placeholder="请输入库存" value="' + data.stock + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item" >' +
                            '            <label class="layui-form-label">吊牌价:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="tagPrice" placeholder="请输入吊牌价" value="' + data.tagPrice + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item" >' +
                            '            <label class="layui-form-label">折扣:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text"  required lay-verify="required" name="discount" placeholder="请输入折扣" value="' + data.discount + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item">' +
                            '            <label class="layui-form-label">折后售价:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="discountPrice" placeholder="请输入折后售价" value="' + data.discountPrice + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '        <div class="layui-form-item" >' +
                            '            <label class="layui-form-label">材质:</label>' +
                            '            <div class="layui-input-inline">' +
                            '                <input type="text" required lay-verify="required" name="clothesMaterial" placeholder="请输入商品材质" value="' + data.clothesMaterial + '"  class="layui-input">' +
                            '            </div>' +
                            '        </div>' +
                            '    </form>'*/
                        , yes: function (index) {
                            var ibody = layer.getChildFrame('body', index);//获取iframe页面body
                            var form = ibody.find('#alterClothesForm');
                            ibody.find("#clothesType").val(ibody.find("#clothesTypeSel").val());
                            ibody.find("#clothesSize").val(ibody.find("#clothesSizeSel").val());
                            var postData = form.serialize();
                            $.ajax({
                                url: '/clothes/edit',
                                type: 'post',
                                data: postData,
                                dataType: 'json',
                                contentType: "application/x-www-form-urlencoded",
                                success: function (data) {
                                    location.reload(true);
                                },
                                error: function () {
                                    alert("编辑错误,请重新编写!");
                                }
                            });
                            layer.close(index);
                        }
                    });
                }
            });
        });

        function add() {
            window.location.href = "/toStockInPage";
        }
/*            layer.open({
                type: 1
                , title: '新增服装'
                , area: '450px'
                , closeBtn: false
                , content: '<form class="layui-form" id="add">' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">商品编号:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="clothesId" placeholder="请输入商品编号"   class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">商品名称:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text"  required lay-verify="required" name="clothesName" placeholder="请输入商品名称"   class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">类型:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required   lay-verify="required" name="clothesType" placeholder="请输入类型"   class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">颜色:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required   lay-verify="required" name="clothesColor" placeholder="请输入颜色"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">尺码:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="clothesSize" placeholder="请输入尺码"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">库存:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text"  required lay-verify="required" name="stock" placeholder="请输入库存"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">吊牌价:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="tagPrice" placeholder="请输入吊牌价"   class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">折扣:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text"  required lay-verify="required" name="discount" placeholder="请输入折扣" value="' + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding: 20px; line-height: 24px;">' +
                    '            <label class="layui-form-label">折后售价:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="discountPrice" placeholder="请输入折后售价" value="' + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="layui-inline" style="padding-left: 20px">' +
                    '            <label class="layui-form-label">材质:</label>' +
                    '            <div class="layui-input-inline">' +
                    '                <input type="text" required lay-verify="required" name="clothesMaterial" placeholder="请输入商品材质" value="' + '"  class="layui-input">' +
                    '            </div>' +
                    '        </div>' +
                    '    </form>'
                , btn: ['确认添加', '取消']
                , btnAlign: 'c'
                , yes: function (index) {
                    $.ajax({
                        url: '/clothes/add',
                        type: 'post',
                        dataType: 'json',
                        contentType: "application/x-www-form-urlencoded",
                        data: $("#add").serialize(),
                        success: function (data) {
                            location.reload(true);
                        },
                        error: function () {
                            alert("添加错误,请重新编写!");
                        }
                    });
                    layer.close(index);
                }
            });*/
