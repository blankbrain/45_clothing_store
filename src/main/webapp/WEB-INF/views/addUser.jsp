<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/4/4
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    //生成表单唯一识别码
    double d = Math.random();
    String flag = Double.toString(d);
    session.setAttribute("flag",flag);
%>
<html>
<head>
    <meta charset="utf-8">
    <title>商品管理</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/addUser.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>



</head>
<body class="layui-layout-body">
<!--页面头部功能模块-->
<%@ include file="head.jsp" %>

<!--页面头部功能模块 end-->
<blockquote class="layui-elem-quote layui-text">
    <h2>添加用户</h2>
</blockquote>
    <div style="padding:1% 20%;">
<form class="layui-form layui-form-pane" style="margin-left: 50px" id="addUserForm" action="${pageContext.request.contextPath}/user/addUser" method="post" onsubmit="return addUserForm()">
    <!--隐藏标签-->
    <input type="hidden" name="flag" value="${sessionScope.flag}" />
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="name" lay-verify="required" autocomplete="off" placeholder="请输入真实姓名" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户名/工号</label>
        <div class="layui-input-inline">
            <input type="text" name="loginname" id="loginname" lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
        </div>
        <label style="color: red;" id="userNameError">${errors.loginname}</label>
    </div>

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-block">
                <input type="password" name="password" id="password" autocomplete="off"  placeholder="密码" class="layui-input">
            </div>
            <label style="color: red;" id="passwordError">${errors.password}</label>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">密码确认</label>
            <div class="layui-input-inline">
                <input type="password" name="password2" id="password2" lay-verify="password|required" autocomplete="off"  placeholder="再次输入密码" class="layui-input">
            </div>
            <label style="color: red;" id="passwordError">${errors.password}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-inline">
            <select class="form-control" name="sex" id="sex">
                <option value="1">女</option>
                <option value="0">男</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">年龄</label>
        <div class="layui-input-inline">
            <input type="tel" name="age" lay-verify="age" placeholder="输入年龄" maxlength="2" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">联系电话</label>
        <div class="layui-input-inline">
            <input type="tel" name="phone" id="phone" lay-verify="phone" placeholder="输入号码" maxlength="11" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户身份</label>
        <div class="layui-input-inline">
            <select name="rid" id="rid">
                <option value="2">收银员</option>
                <option value="1">经理</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item" style="align-items: center">
        <button type="submit" class="layui-btn" lay-submit="addUserForm()">确认添加</button>
        <button class="layui-btn" type="reset">清空</button>
    </div>
    <div style="color: red;font-size: 20px;" align="center">
        <!--错误信息-->${requestScope.addUserError}
    </div>
    <div style="color: green;font-size: 20px;" align="center">
        <!--成功信息-->${requestScope.addUserSucceed}
    </div>
</form>
</div>

<script>
    layui.use(['form','element'], function () {
        var form = layui.form
            ,element = layui.element;
    });

</script>

</body>
</html>
