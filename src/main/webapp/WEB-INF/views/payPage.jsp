<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/5/5
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>销售</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
</head>
<body>
<form class="layui-form layui-form-pane" style="margin-left: 100px;margin-top: 30px">

    <div class="layui-form-item">
        <label class="layui-form-label">应收金额：</label>
        <div class="layui-input-inline">
            <input type="text" id="countMoney" lay-verify="required" readonly="readonly" value="${sessionScope.ORDER_SESSION.amount}" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label>支付方式：</label>
        <div>
        <table class="layui-table" style="width: 300px;height: 100px">
            <tr>
                <td><img src="${pageContext.request.contextPath}/images/bankPay.jpg" style="width: 100px;height: 50px"></td>
                <td><input type="radio" name="paystyle" id="bankPay" value="2"></td>
            </tr>
            <tr>
                <td><img src="${pageContext.request.contextPath}/images/weChatPay.jpg" style="width: 100px;height: 50px"></td>
                <td><input type="radio" name="paystyle" id="weChatPay" value="3"></td>
            </tr>
            <tr>
                <td><img src="${pageContext.request.contextPath}/images/aliPay.jpg" style="width: 100px;height: 50px"></td>
                <td><input type="radio" name="paystyle" id="aliPay" value="4"></td>
            </tr>

        </table>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">实收金额</label>
        <div class="layui-input-inline">
            <input type="text" id="realMoney" lay-verify="required" placeholder="请输入实收金额" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">找零</label>
        <div class="layui-input-inline">
            <input type="text" id="makeChange" lay-verify="required"  readonly="readonly" class="layui-input">
        </div>
    </div>

</form>
<script>
    //JavaScript代码区域
    layui.use(['element','table'], function() {
        var element = layui.element;
            var table = layui.table;
            table.render();
    });
</script>
</body>
</html>
