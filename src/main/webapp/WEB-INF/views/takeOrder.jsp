<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/4/22
  Time: 8:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>未处理订单</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js/"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/takeOrder.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
</head>

<body class="layui-layout-body">

<jsp:include page="head.jsp" />
<div class="layui-fluid" style="margin-top: 30px">
    <div style="padding:1% 15%;">
<div class="layui-row">

    <div class="layui-col-md4">
        <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-input-inline">
                <input type="text" name="telphone" id="telphone" placeholder="请输入手机号/手机尾号" lay-verify="number" oninput="value=value.replace(/[^\d]/g,'')" class="layui-input"  style="width:180px">
            </div>
            <button class="layui-btn" lay-submit lay-filter="search">
                <i class="layui-icon">&#xe615;</i>搜索
            </button>
        </div>
        </form>
    </div>

    <div class="layui-col-md8">
        <button class="layui-btn layui-btn-warm"  onclick="getAll()" id="sousuo">
            <i class="layui-icon">&#xe615;</i>查询全部
        </button>
        <button class="layui-btn layui-btn-warm"  onclick="batchDelPendingOrder()" id="shanchu">
            <i class="layui-icon">&#xe640;</i>删除
        </button>
    </div>
</div>

        <table id="ordersDisplay" lay-filter="test"></table>

    </div>
</div>

<script type="text/html" id="barDemo">
    <%--<a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>--%>
    <a class="layui-btn layui-btn layui-btn-xs" lay-event="take">取单</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;

    });
</script>

<%--<script>
    layui.use(['form','table'], function () {
        var form = layui.form
            ,table = layui.table;

         /*验证*/
        form.verify({
            phone:[/^1[3|4|5|7|8]\d{9}$/, '手机必须11位，只能是数字！']
        })

        /*展示搜索到的订单*/
        table.render({
            elem:'#ordersDisplay'
            ,url:'#'
            ,title:'订单展示'
            ,cols:[[
                {field:'orderId',title:'订单编号',width:120,fix:'left',align:'center'}
                ,{field:'phone', title:'手机号', width:150,align:'center'}
                ,{field:'orderDate', title:'生成时间', width:120,sort:true,align:'center',edit:'text'}
                ,{field:'salesman', title:'销售人员', width:120, align:'center'}
                ,{field:'totalPrice', title:'总金额', width:120}
                ,{field: 'barDemo', width:180 , align:'center', title: '常用操作', toolbar: '#barDemo'}
            ]]
            ,page:true
        });

        //监听工具条
        table.on('tool(test)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){


            } else if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'take'){


            }
        });

    });

</script>--%>

</body>
</html>
