<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/5/4
  Time: 22:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">

    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>

    <script>
        $(document).ready(function() {
            var userVo = "<%=request.getAttribute("UserVoMsg") %>";
/*            alert(userVo.roleId);*/
            var roleId = parseInt($("#roleid").val());
            <!--修改时间格式-->
            var createdata = $("#createdate").val();
            $("#createdate").val(createTime(createdata));
/*            alert("时间为：" + $("#createdate").val());*/
            <!--获取已有角色-->
            if (roleId===1) {
                $("#roleId").prepend("<option value='1'>经理</option>");
                $("#roleId").append("<option value='2'>收银员</option>");
                $("#roleId").val("1");
            } else if (roleId === 2) {
                $("#roleId").prepend("<option value='2'>收银员</option>");
                $("#roleId").append("<option value='1'>经理</option>");
                $("#roleId").val("2");
            }
        });
        <!--修改时间格式-->
        function createTime(v){
            var date = new Date(v);
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            m = m<10?'0'+m:m;
            var d = date.getDate();
            d = d<10?("0"+d):d;
            var h = date.getHours();
            h = h<10?("0"+h):h;
            var M = date.getMinutes();
            M = M<10?("0"+M):M;
            var str = y+"-"+m+"-"+d+" "+h+":"+M;
            return str;
        }
    </script>
</head>
<body class="layui-layout-body">
<input type="hidden" id="roleid" value="${requestScope.UserVoMsg.roleId}">
 <form class="layui-form layui-form-pane" style="margin-left: 50px" id="alterUserForm" action="${pageContext.request.contextPath}/user/addUser" method="post">
     <!--隐藏标签-->
     <input type="hidden" name="id" value="${requestScope.UserVoMsg.id}" />
     <div class="layui-form-item" style="margin-top: 15px">
         <label class="layui-form-label">姓名</label>
         <div class="layui-input-inline">
             <input type="text" name="name" lay-verify="required" autocomplete="off" placeholder="请输入真实姓名" value="${requestScope.UserVoMsg.name}" class="layui-input">
         </div>
     </div>
     <div class="layui-form-item">
         <label class="layui-form-label">用户名/工号</label>
         <div class="layui-input-inline">
             <input type="text" name="loginname" id="loginname" lay-verify="required" placeholder="请输入用户名" readonly="readonly" value="${requestScope.UserVoMsg.loginname}" autocomplete="off" class="layui-input">
         </div>
     </div>
     <input type="hidden" name="password" value="${requestScope.UserVoMsg.password}" />
     <div class="layui-form-item">
         <label class="layui-form-label">性别</label>
         <div class="layui-input-inline">
             <input type="text" name="sex" id="sex" lay-verify="true" autocomplete="off"  readonly="readonly" value="${requestScope.UserVoMsg.sex}" class="layui-input">
         </div>
     </div>
     <div class="layui-form-item">
         <label class="layui-form-label">年龄</label>
         <div class="layui-input-inline">
             <input type="text" name="age" lay-verify="age" placeholder="请输入年龄" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" value="${requestScope.UserVoMsg.age}" class="layui-input">
         </div>
     </div>
     <div class="layui-form-item">
         <label class="layui-form-label">联系电话</label>
         <div class="layui-input-inline">
             <input type="text" name="phone" id="phone" lay-verify="phone|required" placeholder="请输入号码" oninput="value=value.replace(/[^\d]/g,'')" autocomplete="off" value="${requestScope.UserVoMsg.phone}" class="layui-input">
         </div>
     </div>
     <div class="layui-form-item">
         <label class="layui-form-label">创建时间</label>
         <div class="layui-input-inline">
             <input type="text" name="createdate" id="createdate" readonly="readonly" value="${requestScope.UserVoMsg.createdate}" class="layui-input">
         </div>
     </div>
     <input type="hidden" name="roleName" id="roleName" value="${requestScope.UserVoMsg.roleName}" />
     <div class="layui-form-item">
         <label class="layui-form-label">用户身份</label>
         <div class="layui-input-inline">
             <select name="roleId" id="roleId">
<%--                 <option value="0"></option>--%>
<%--                 <option value="2">收银员</option>
                 <option value="1">经理</option>--%>
             </select>
         </div>
     </div>

 </form>

 <script>
     layui.use(['form','element'], function () {
         var form = layui.form
             ,element = layui.element;
     });
 </script>
</body>
</html>
