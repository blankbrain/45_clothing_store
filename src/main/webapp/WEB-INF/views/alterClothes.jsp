<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/6/8
  Time: 20:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">

    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/alterClothes.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>

</head>
<body class="layui-layout-body">

<form class="layui-form layui-form-pane" style="margin-left: 50px" id="alterClothesForm">
    <input type="hidden" id="clothesType" name="clothesType" value="${requestScope.clothes.clothesType}"/>
    <input type="hidden" id="clothesSize" name="clothesSize" value="${requestScope.clothes.clothesSize}">
                <div class="layui-form-item" style="margin-top: 15px">
                   <label class="layui-form-label">商品编号:</label>
                    <div class="layui-input-inline">
                            <input type="tel" required lay-verify="required" name="clothesId" id="clothesId" placeholder="请输入商品编号" maxlength="20" oninput="value=value.replace(/[^\d]/g,'')" value="${requestScope.clothes.clothesId}" readonly="readonly" class="layui-input">
                    </div>
                </div>
            <div class="layui-form-item">
                    <label class="layui-form-label">商品名称:</label>
                    <div class="layui-input-inline">
                            <input type="text"  required lay-verify="required" name="clothesName" id="clothesName" placeholder="请输入商品名称" value="${requestScope.clothes.clothesName}"  class="layui-input">
                        </div>
                </div>
    <div class="layui-form-item">
        <label class="layui-form-label">类型</label>
        <div class="layui-input-inline">
            <select name="clothesTypeSel" id="clothesTypeSel">
            </select>
        </div>
    </div>
            <div class="layui-form-item">
                    <label class="layui-form-label">颜色:</label>
                    <div class="layui-input-inline">
                            <input type="text" required   lay-verify="required" name="clothesColor" id="clothesColor" placeholder="请输入颜色" value="${requestScope.clothes.clothesColor}" class="layui-input">
                        </div>
                </div>
    <div class="layui-form-item">
        <label class="layui-form-label">尺码</label>
        <div class="layui-input-inline">
            <select name="clothesSizeSel" id="clothesSizeSel" lay-filter="sizeRender">
                <option value="XS">XS</option>
                <option value="S">S</option>
                <option value="M">M</option>
                <option value="L">L</option>
                <option value="XL">XL</option>
                <option value="XXL">XXL</option>
                <option value="XXXL">XXXL</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">库存:</label>
    <div class="layui-input-inline">
        <%--<input id="changeNum" name="num" class="alignment" data-edit="true" value="1"/>--%>
        <%--数量增减模块--%>
        <input id="dec" class="layui-btn" type="button" value="-">
        <input id="stock" lay-verify="number" type="text" name="stock" oninput="value=value.replace(/[^\d]/g,'')" value="${requestScope.clothes.stock}" size="1" style="text-align:center;width:60px;height: 40px">
        <input id="inc" class="layui-btn" type="button" value="+">
    </div>
    </div>


            <div class="layui-form-item">
                    <label class="layui-form-label">吊牌价:</label>
                    <div class="layui-input-inline">
                            <input required lay-verify="required" name="tagPrice" id="tagPrice" placeholder="请输入吊牌价" type='number' onblur="if (!/^\d+(\.\d+)?$/.test(this.value)){alert('只能输入数字且保留两位小数');this.value='';}" step="0.01" value="${requestScope.clothes.tagPrice}" class="layui-input">
                        </div>
                </div>
            <div class="layui-form-item">
                    <label class="layui-form-label">折扣:</label>
                    <div class="layui-input-inline">
                        <input type="tel" name="discount" id="discount"  placeholder="请输入折扣" maxlength="2" value="${requestScope.clothes.discount}" class="layui-input">
                        </div>
                </div>
            <div class="layui-form-item">
                    <label class="layui-form-label">折后售价:</label>
                    <div class="layui-input-inline">
                            <input type="text" id="discountPrice" name="discountPrice" readonly="readonly" placeholder="请输入折后售价" value="${requestScope.clothes.discountPrice}"  class="layui-input">
                       </div>
                </div>
            <div class="layui-form-item">
                    <label class="layui-form-label">材质:</label>
                    <div class="layui-input-inline">
                            <input type="text" lay-verify="required" name="clothesMaterial" placeholder="请输入商品材质" value="${requestScope.clothes.clothesMaterial}"  class="layui-input">
                        </div>
                </div>
        </form>
<script>
    layui.use(['form','element'], function () {
        var form = layui.form
            ,element = layui.element;
    });
</script>
</body>
</html>
