<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/4/8
  Time: 13:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>用户管理</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js/"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/userPopUp.js"></script>
</head>
<body class="layui-layout-body">
<!--页面头部功能模块-->
<%@ include file="head.jsp" %>

<!--页面头部功能模块 end-->
<blockquote class="layui-elem-quote layui-text">
<h2>用户信息</h2>
</blockquote>
<!--内容主区域-->
<div style="padding:1% 15%;">
    <!--数据表格-->
    <table class="layui-hide" lay-filter='demo' id="userTable"></table>



    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
        &nbsp;
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        &nbsp;
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<script>
    layui.use(['form','element'], function () {
        var form = layui.form
            ,element = layui.element;
    });

</script>
<!--获取后台所有数据-->
<script>
    layui.use(['table','layer'], function() {
        var table = layui.table
            ,layer = layui.layer;
        //方法级渲染
        table.render({
            elem: '#userTable'
            ,url: '${pageContext.request.contextPath}/user/find'
            ,id: 'userReload'
            ,page: true
            ,height:500
            ,cols: [[
                {field:'loginname', title: '账号/工号',  sort: true,align:'center'}
                ,{field:'name', title: '姓名',  align:'center'}
                ,{field:'roleName', title: '身份',  align:'center'}
                ,{field:'sex', title: '性别', align:'center'}
                ,{field:'phone', title: '联系方式',  align:'center'}
                ,{field:'createdate', title:'生成时间', sort:true,align:'center',hide:true,
                    templet :function (row){
                        return createTime(row.createdate);
                    }}
                ,{field: 'barDemo', align:'center', title: '操作', toolbar: '#barDemo'}
            ]]
        });

    })

    /**
     * layui表格的创建时间字段转化成时间格式
     * @param v
     * @returns {string}
     */
    function createTime(v){
        var date = new Date(v);
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        m = m<10?'0'+m:m;
        var d = date.getDate();
        d = d<10?("0"+d):d;
        var h = date.getHours();
        h = h<10?("0"+h):h;
        var M = date.getMinutes();
        M = M<10?("0"+M):M;
        var str = y+"-"+m+"-"+d+" "+h+":"+M;
        return str;
    }

</script>
</body>
</html>
