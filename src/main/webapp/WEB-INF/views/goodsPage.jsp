<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<meta charset="utf-8">
	<title>商品管理</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js/"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>

	<style>
		.layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
			top: 50%;
			transform: translateY(-50%);
		}
	</style>


</head>
<body class="layui-layout-body">
<!--页面头部功能模块-->
<%@ include file="head.jsp" %>

<!--页面头部功能模块 end-->

<!--内容主区域-->
<div style="padding:1% 10%;">

	<shiro:hasPermission name="clothes:add">
		<button class="layui-btn layui-btn-danger" style="margin-top:10px;margin-left:2px" onclick="add()" >
			<i class="layui-icon">&#xe608;</i>添加服装
		</button>
	</shiro:hasPermission>

		<div class="layui-input-inline" style="margin-top:10px;">
			<input type="text" name="keyWord" id="keyWord" required lay-verify="required" class="layui-input" placeholder="请输入搜索内容"/>
		</div>
		<div class="layui-input-inline" style="margin-top:10px;">
                    <select name="keyType" id="key_type">
                        <option value="">选择搜索内容</option>
                        <option value="clothesId">按服装编号</option>
                        <option value="clothesName">按服装名称</option>
                        <option value="clothesType">按分类</option>
                    </select>
            </div>
		<button class="layui-btn" style="margin-top:10px;" data-type="reload" id="sousuo">
			<i class="layui-icon">&#xe615;</i>搜索
		</button>

<shiro:hasPermission name="clothes:batchDelete">
		<button class="layui-btn layui-btn-warm" style="margin-top:10px;" data-type="batchDelete" id="shanchu">
			<i class="layui-icon">&#xe640;</i>删除
		</button>
</shiro:hasPermission>

<%--	</form>--%>

		<!--数据表格-->
		<table class="layui-hide" lay-filter='demo' id="goodsTable"></table>



		<script type="text/html" id="barDemo">
			<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
<shiro:hasPermission name="clothes:edit">
			<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
</shiro:hasPermission>
			<shiro:hasPermission name="clothes:delete">
			<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</shiro:hasPermission>
		</script>
		<!--弹窗-->
		<script type="application/javascript" src="${pageContext.request.contextPath}/js/pages/goodsPopUp.js"></script>
</div>

<!--内容主区域end-->


<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;

    });
</script>

<!--获取后台所有数据-->
<script>
    layui.use(['table','layer'], function() {
        var table = layui.table
            ,layer = layui.layer;
        //方法级渲染
        table.render({
            elem: '#goodsTable'
            ,url: '${pageContext.request.contextPath}/clothes/find'
            ,id: 'goodsReload'
            ,page: true
            ,height:450
            ,limit:6
            ,limits:[8,10,12,15]
            ,cols: [[
                {checkbox: true, fixed: true }
                ,{field:'clothesId', title: '商品编号', sort: true,align:'center'}
                ,{field:'clothesName', title: '商品名称',align:'center'}
                ,{field:'clothesType', title: '类型',  align:'center'}
                ,{field:'clothesColor', title: '颜色',  align:'center'}
                ,{field:'clothesSize', title: '尺码',  align:'center'}
                ,{field:'stock', title: '库存', sort: true,align:'center'}
                ,{field:'tagPrice', title: '吊牌价', align:'center'}
                ,{field:'discount', title: '折扣', align:'center'}
                ,{field:'discountPrice', title: '折后售价', align:'center'}
                ,{field: 'barDemo', align:'center', title: '操作', toolbar: '#barDemo'}
            ]]
        });

        <!--搜索功能-->
        var $ = layui.$, active = {

            reload: function () {

                //获取select选中的值
                var keyWord=$("#keyWord").val();
                var keyType=$("#key_type option:selected").val();

                //执行重载
                table.reload('goodsReload', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , where: {

                        keyWord:keyWord,
                        keyType:keyType

                    }
                });
            },

			<!--批量删除功能-->
            batchDelete: function () {
                var checkStatus = table.checkStatus('goodsReload')
                    ,data = checkStatus.data;
                var str = "" ;
                if(data.length>0){
                    for(var i=0; i<data.length; i++){
                        str+=data[i].clothesId+",";
                    }
                    layer.confirm('是否删除这'+data.length+'数据',{icon:3,title:'提示'},function (index) {
                        $.ajax({
                            url:'/clothes/batchDelete',
                            type:'post',
                            data:{
                                'str' : str
                            },
                            dataType:'json',
                            success:function(){
                                location.reload(true);
                            },
                            error:function () {
                                alert("删除错误,请重新编写!");
                            }
                        });
                        layer.close(index);
                    });
                }else{
                    layer.alert("请选择要删除的数据!")
                }
            }
        };
        $('#sousuo').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        $('#shanchu').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
    });
</script>
<%--<form class="layui-form layui-form-pane" style="margin-left: 50px">
	<div class="layui-form-item">
		<label class="layui-form-label">用户身份</label>
		<div class="layui-input-inline">
			<select name="rid" id="rid">
				<option value="1">经理</option>
				<option value="2">收银员</option>
			</select>
		</div>
	</div>
</form>--%>
</body>
</html>
