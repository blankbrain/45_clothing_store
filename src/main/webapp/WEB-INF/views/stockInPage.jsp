<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/6/7
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <title>商品入库</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/stockInPage.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
</head>
<body class="layui-layout-body">
<!--页面头部功能模块-->
<%@ include file="head.jsp" %>

<!--页面头部功能模块 end-->
<blockquote class="layui-elem-quote layui-text">
    <h2>商品入库</h2>
</blockquote>
<div style="padding:1% 20%;">
    <form class="layui-form layui-form-pane" style="margin-left: 50px" id="addUserForm" >

        <div class="layui-col-md6">
        <div class="layui-form-item">
            <label class="layui-form-label">商品编号</label>
            <div class="layui-input-inline">
                <input type="tel" name="clothesId" id="clothesId" lay-verify="required" autocomplete="off" maxlength="12" oninput="value=value.replace(/[^\d]/g,'')" placeholder="请输入服装编号" class="layui-input">
            </div>
            <label style="color: red;" id="clothesIdMsg"></label>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">入库数量</label>
            <div class="layui-input-inline">
                <input id="dec" class="layui-btn" type="button" value="-" >
                <input id="num" lay-verify="number" type="text" name="num" oninput="value=value.replace(/[^\d]/g,'')" value="1" size="1" style="text-align:center;width:60px;height: 40px">
                <input id="inc" class="layui-btn" type="button" value="+">
            </div>
            <label style="color: red;" id="numMsg"></label>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">商品名称</label>
            <div class="layui-input-inline">
                <input type="text" name="clothesName" id="clothesName" lay-verify="required" placeholder="请输入服装名称" autocomplete="off" class="layui-input">
            </div>
            <label style="color: red;" id="clothesNameMsg"></label>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline">
                <select name="clothesType" id="clothesType">
                </select>
            </div>
        </div>

            <div class="layui-form-item">
                <label class="layui-form-label">尺码</label>
                <div class="layui-input-inline">
                    <select name="clothesSize" id="clothesSize" lay-filter="sizeRender">
                        <option value="XS">XS</option>
                        <option value="S">S</option>
                        <option value="M">M</option>
                        <option value="L">L</option>
                        <option value="XL">XL</option>
                        <option value="XXL">XXL</option>
                        <option value="XXXL">XXXL</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="layui-col-md6">

        <div class="layui-form-item">
            <label class="layui-form-label">吊牌价</label>
            <div class="layui-input-inline">
                <input type="number" name="tagPrice" id="tagPrice" placeholder="请输入吊牌价" type='number' onblur="if (!/^\d+(\.\d+)?$/.test(this.value)){alert('只能输入数字且保留两位小数');this.value='';}" step="0.01"   class="layui-input">
            </div>
            <label style="color: red;" id="tagPriceMsg"></label>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">折扣</label>
            <div class="layui-input-inline">
                <input type="tel" name="discount" id="discount"  placeholder="请输入折扣" maxlength="2"  class="layui-input">
            </div>
            <label style="color: red;" id="discountMsg"></label>
        </div>


        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">折后售价</label>
                <div class="layui-input-block">
                    <input type="text" name="discountPrice" id="discountPrice"  readonly="readonly" class="layui-input">
                </div>
            </div>
        </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="clothesColor" id="clothesColor" lay-verify="password|required" autocomplete="off"  placeholder="请输入颜色" class="layui-input">
                    </div>
                    <label style="color: red;" id="clothesColorMsg"></label>
                </div>
            </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">材质</label>
                <div class="layui-input-block">
                    <input type="test" name="clothesMaterial" id="clothesMaterial" autocomplete="off"  placeholder="请输入材质" class="layui-input">
                </div>
                <label style="color: red;" id="clothesMaterialMsg"></label>
            </div>
        </div>

            <div class="layui-form-item" style="align-items: center">
                <button id="stockInbtn" type="button" class="layui-btn" onclick="stockIn()">入库</button>
                <button class="layui-btn" type="reset" onclick="resetAll()">清空</button>
            </div>

        </div>

        <div style="color: red;font-size: 20px;" align="center">
            <!--错误信息-->${requestScope.addUserError}
        </div>
        <div style="color: green;font-size: 20px;" align="center">
            <!--成功信息-->${requestScope.addUserSucceed}
        </div>
    </form>
</div>

<script>
    layui.use(['form','element'], function () {
        var form = layui.form
            ,element = layui.element;
    });
    window.onload = function() {
        $('#inc').click(function() {
            $('#num').val(parseInt($('#num').val()) + 1);
        });
        $('#dec').click(function() {
            if( $('#num').val()>1)
                $('#num').val(parseInt($('#num').val()) - 1);
        });
    };
</script>
</body>
</html>
