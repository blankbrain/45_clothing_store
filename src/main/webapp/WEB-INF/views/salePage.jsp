<%@ page import="com.ClothingStorePOS.pojo.Order" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/4/12
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>销售</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
</head>
<body class="layui-layout-body">
<%@ include file="head.jsp" %>

<div class="layui-fluid">
    <div class="layui-row layui-col-space20" style="margin-top: 20px">

     <%--    商品展示部分    --%>
        <div style="padding:1% 5%;">
        <div class="layui-col-md8" style="padding: 10px 10px 10px 10px">

            <div class="layui-row">
            <div class="layui-col-md3">
            <button class="layui-btn" lay-filter="demo2" data-type="makeNewSale" id="makeNewSale" type="button" onclick="makeNewSale()">开启一次新的销售</button>
            </div>

            <div class="layui-col-md9">
            <form class="layui-form" action="" id="addForm">
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" name="clothesid" id="enterId" placeholder="请输入商品编号" oninput="value=value.replace(/[^\d]/g,'')" autocomplete="off" class="layui-input" lay-verify="number" style="width:180px">
                    </div>
                    <button class="layui-btn" lay-submit lay-filter="confirm" id="confirm" disabled="disabled">确定</button>
                    <div class="layui-input-inline">
                    <%--<input id="changeNum" name="num" class="alignment" data-edit="true" value="1"/>--%>
                        <%--数量增减模块--%>
                        <input id="dec" class="layui-btn" type="button" value="-" disabled="disabled">
                        <input id="count" lay-verify="number" type="text" name="num" oninput="value=value.replace(/[^\d]/g,'')" value="1" size="1" style="text-align:center;width:60px;height: 40px">
                        <input id="inc" class="layui-btn" type="button" value="+" disabled="disabled">
                    </div>
                </div>
            </form>
            </div>
            </div>

            <!--表格区-->
            <div class="layui-row">
                <table class="layui-hide" name="buyItems" id="buyItems" lay-filter="sale"></table>
            </div>

            <!--结算区-->
<%--            <div class="layui-col-md4">
            <form class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">总计</label>
                        <div class="layui-input-inline">
                            <input type="text" name="amount" id="amount" readonly="readonly" value="${sessionScope.ORDER_SESSION.getAmount()}" class="layui-input">
                        </div>
                    </div>
                </div>
            </form>
                </div>--%>
            <div class="layui-row">
                <div class="layui-col-md8"></div>
                <div class="layui-col-md4">
                <button class="layui-btn" id="pendingOrder" type="button" onclick="pendingOrder()" disabled="disabled">挂单</button>
                <button class="layui-btn" id="chargeMoney" type="button" onclick="chargeMoney()" disabled="disabled">结算</button>
                    <button class="layui-btn" id="cancelOrder" type="button" onclick="cancelOrder()" disabled="disabled">全单取消</button>
                </div>
            </div>

        </div>
        </div>
         <%-- 商品展示部分end--%>

         <%--商品搜索部分--%>

        <div class="layui-col-md4">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">尺码</label>
                    <div class="layui-input-inline">
                        <select name="clothesSize" id="clothesSize">
                            <option value="1">XS</option>
                            <option value="2">S</option>
                            <option value="3">M</option>
                            <option value="4">L</option>
                            <option value="5">XL</option>
                            <option value="6">XXL</option>
                            <option value="7">XXXL</option>
                        </select>
                    </div>
                    <button id="search" type="button" class="layui-btn" onclick="searchStock()">搜索同款服装</button>
                </div>
                <table class="layui-hide" id="searchItems" lay-filter="test"></table>
            </form>
        </div>
        <%--商品搜索部分end--%>

    </div>
</div>

<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;

    });
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/salePage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/payPage.js"></script>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">修改数量</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<!--jQuery计数插件-->
<script>
    // 自定义类型：参数为数组，可多条数据
   /* alignmentFns.createType([{"test": {"step" : 10, "min" : 10, "max" : 999, "digit" : 0}}]);*/

/*    // 初始化
    alignmentFns.initialize();

    // 销毁
    alignmentFns.destroy();

    // js动态改变数据
    $("#changeNum").attr("data-step", "1");
    $("#changeNum").attr("data-digit", "0");
    $("#changeNum").attr("data-min", "1");
    $("#changeNum").attr("data-max", "9999999");

    // 初始化
    alignmentFns.initialize();*/
    <%--计数模块--%>
    window.onload = function() {
        $('#dec').click(function() {
            if( $('#count').val()>1)
                $('#count').val(parseInt(count.value) - 1);
        });
        $('#inc').click(function() {
            $('#count').val(parseInt(count.value) + 1);
        });
    };

</script>

</body>
</html>
