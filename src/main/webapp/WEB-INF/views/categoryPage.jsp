<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/3/25
  Time: 23:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <meta charset="utf-8">
    <title>商品管理</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/num-alignment.js"></script>
    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/categoryPage.js"></script>

    <style>
        .layui-table-cell .layui-form-checkbox[lay-skin="primary"]{
            top: 50%;
            transform: translateY(-50%);
        }
    </style>

</head>
<body class="layui-layout-body">
<!--页面头部功能模块-->
<jsp:include page="head.jsp" />

<!--页面头部功能模块 end-->

<script>
    function add(){
        layer.open({
            type: 1
            , title: '新增服装类别'
            , area: '350px'
            , closeBtn: false
            , content: '<form class="layui-form layui-form-pane" id="add">' +
                '        <div class="layui-form-item" style="padding: 20px; line-height: 24px;">' +
                '            <label class="layui-form-label">服装类别:</label>' +
                '            <div class="layui-input-inline">' +
                '                <input type="text" required lay-verify="required" name="type" placeholder="请输入服装类别"   class="layui-input">' +
                '            </div>' +
                '        </div>' +
                '    </form>'
            , btn: ['确认添加', '取消']
            , btnAlign: 'c'
            , yes: function (index) {
                $.ajax({
                    url: '/category/add',
                    type: 'post',
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded",
                    data: $("#add").serialize(),
                    success: function (data) {
                        location.reload(true);
                    },
                    error: function () {
                        alert("该类型已存在,请重新编写!");
                    }
                });
                layer.close(index);
            }
        });
    }
</script>

<!--内容主区域-->

<div style="padding:1% 15%;">
    <div style="padding:1% 1%;">
        <div class="row">
            <div class="col-md-3">
                <!-- <div class="form-group" style="padding-left: 2%;padding-right: 2%;"> -->

                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                        <select name="categoryItem" id="categoryItem" lay-verify="required" lay-search="" class="form-control" lay-filter="lf">
                            <option value="">选择服装类型</option>
                        </select>
                            </div>
                        </div>
                </div>

            </div>
            <!--按钮组-->
            <div class="col-md-8">
                <div class="layui-btn-group demoTable">
                    <button class="layui-btn" data-type="reload" id="sousuo"><i class="layui-icon">&#xe615;</i>搜索</button>
            <shiro:hasPermission name="category:alterChoose">
                    <button class="layui-btn" data-type="alter" id="alterType">修改该类型</button>
            </shiro:hasPermission>
                    <shiro:hasPermission name="category:add">
                    <button class="layui-btn" onclick="add()" >新增服装类型</button>
            </shiro:hasPermission>
                </div>
            </div>
            <!--按钮组end-->

        </div>
    </div>
    <!--数据表格-->
    <table class="layui-hide" lay-filter='demo' id="categoryTable"></table>
</div>
<!--内容主区域end-->

<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;

    });
</script>

</body>
</html>

