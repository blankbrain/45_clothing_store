<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>经营状况展示</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}images/bookico.jpg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.min.js"></script>

    <!--js代码-->
    <script src="${pageContext.request.contextPath}/js/jedate/jedate.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/lbj.js"></script>
    <script src="http://cdn.highcharts.com.cn/highcharts/highcharts.js"></script>


</head>
<body>
<jsp:include page="head.jsp" />

<div class="layui-fluid">
    <%--总体销售情况统计--%>
    <blockquote class="layui-elem-quote layui-text">
        <h2>销售展示</h2>
    </blockquote>
    <div class="layui-row">

        <div class="layui-col-md6">
            <div class="layui-row">
            <div class="layui-form-item">
                <label class="layui-form-mid">商品类别：</label>
                <div class="layui-input-inline">
                    <select class="form-control" name="clothClass" id="clothClass">
                        <option value="0">请选择服装类别</option>
                    </select>
                </div>
<%--                <button class="layui-btn" onclick="chart1Data()">确定</button>--%>
            </div>
                <div class="layui-form-item">
                    <label class="layui-form-mid">时间范围：</label>
                    <%--                <div class="layui-input-inline">
                                        <input type="text" class="layui-input" id="startDate" placeholder="开始时间">
                                    </div>
                                    <label class="layui-form-mid">至</label>
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input" id="endDate" placeholder="结束时间">
                                    </div>--%>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" id="dateRange" placeholder="请选择起始和结束时间">
                    </div>
                </div>

            </div>
            <%--插入指定时间段内销售统计--%>
            <div id="saleChart1" style="min-width:400px;height: 350px"></div>


        </div>
        <div class="layui-col-md6">
                <div class="layui-form-item">
                    <label class="layui-form-mid">商品名称：</label>
                    <div class="layui-input-inline">
                        <%--<input type="text" class="layui-input" id="clothType" placeholder="请选择服装类别">--%>
                        <select class="form-control" name="clothType" id="clothType">
                            <option value="0">请选择服装类别</option>
                        </select>
                    </div>
                    <div class="layui-input-inline">
<%--                        <input type="text" class="layui-input" id="clothName" placeholder="请选择服装名称">--%>
                        <select class="form-control" name="clothName" id="clothName">
                            <option value="0">请选择服装名称</option>
                        </select>
                    </div>
<%--                    <button class="layui-btn" onclick="chart2Data()">确定</button>--%>
                </div>
            <div class="layui-form-item">
                <label class="layui-form-mid">选择年份：</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="saleYear1" placeholder="查询年份">
                </div>
            </div>

            <%--某商品指定年份的月销售量--%>
            <div id="saleChart2" style="max-width:800px;height:350px"></div>

        </div>
    </div>


    <blockquote class="layui-elem-quote layui-text">
        <h2>营业额展示</h2>
    </blockquote>
    <div class="layui-row">
        <div class="layui-col-md6 layui-col-md-offset3">
        <div class="layui-form-item">
            <label class="layui-form-mid">查看年份：</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="saleYear2" placeholder="查询年份">
            </div>
        </div>
        <%--季度销售额柱状图--%>
        <div id="saleChart3" style="max-width:800px;height:350px;"></div>
        </div>
    </div>

</div>
<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;

    });
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/saleDisplay.js"></script>

</body>
</html>
