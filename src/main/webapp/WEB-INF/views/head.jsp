<%--
  Created by IntelliJ IDEA.
  User: YQ
  Date: 2019/3/21
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%--页面头部公共部分 --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo"><b>服装店POS系统</b></div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item">
                <a href="javascript:;">前台销售</a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="${pageContext.request.contextPath}/toSalePage">商品销售</a>
                    </dd>
                    <dd>
                        <a href="${pageContext.request.contextPath}/toTakeOrderPage">未处理订单</a>
                    </dd>
<%--                    <dd>
                        <a href="${pageContext.request.contextPath}goodsPage.action">订单查询</a>
                    </dd>--%>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">库存管理</a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="${pageContext.request.contextPath}/toStockInPage">采购入库</a>
                    </dd>
                    <dd>
                        <a href="${pageContext.request.contextPath}/toCategoryPage">类别管理</a>
                    </dd>

                    <dd>
                        <a href="${pageContext.request.contextPath}/toClothesPage">商品管理</a>
                    </dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">销售记录</a>
                <dl class="layui-nav-child">
<shiro:hasPermission name="page:toSaleDisplayPage">
                    <dd>
                        <a href="${pageContext.request.contextPath}/toSaleDisplayPage">销售统计</a>
                    </dd>
</shiro:hasPermission>
<%--                    <dd>
                        <a href="${basePath}openStockOutLog.action">入库记录</a>
                    </dd>--%>
                </dl>
            </li>
<shiro:hasPermission name="page:toUserPage">
            <li class="layui-nav-item">
                <a href="javascript:;">用户管理</a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="${pageContext.request.contextPath}/toAddUserPage">添加用户</a>
                    </dd>
                    <dd>
                        <a href="${pageContext.request.contextPath}/toUserPage">用户信息</a>
                    </dd>
                </dl>
            </li>
</shiro:hasPermission>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">

                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img"><%--<shiro:principal/>--%>
                </a>
            </li>
            <li class="layui-nav-item">
                <a href="${pageContext.request.contextPath}/user/logout">退出</a>
            </li>
        </ul>
    </div>
</div>
