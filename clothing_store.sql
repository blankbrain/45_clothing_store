/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : clothing_store

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-06-20 23:15:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for role_resource
-- ----------------------------
DROP TABLE IF EXISTS `role_resource`;
CREATE TABLE `role_resource` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(20) DEFAULT NULL,
  `resource_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `role_resource_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `role_resource_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `tb_resource` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_resource
-- ----------------------------
INSERT INTO `role_resource` VALUES ('1', '1', '1');
INSERT INTO `role_resource` VALUES ('2', '1', '2');
INSERT INTO `role_resource` VALUES ('3', '1', '3');
INSERT INTO `role_resource` VALUES ('4', '1', '4');
INSERT INTO `role_resource` VALUES ('5', '1', '5');
INSERT INTO `role_resource` VALUES ('6', '1', '6');
INSERT INTO `role_resource` VALUES ('7', '1', '7');
INSERT INTO `role_resource` VALUES ('8', '1', '8');
INSERT INTO `role_resource` VALUES ('9', '1', '9');
INSERT INTO `role_resource` VALUES ('10', '1', '10');
INSERT INTO `role_resource` VALUES ('11', '1', '11');
INSERT INTO `role_resource` VALUES ('12', '1', '12');
INSERT INTO `role_resource` VALUES ('13', '1', '13');
INSERT INTO `role_resource` VALUES ('14', '1', '14');
INSERT INTO `role_resource` VALUES ('15', '1', '15');
INSERT INTO `role_resource` VALUES ('16', '1', '16');
INSERT INTO `role_resource` VALUES ('17', '1', '17');
INSERT INTO `role_resource` VALUES ('18', '3', '1');
INSERT INTO `role_resource` VALUES ('19', '3', '2');
INSERT INTO `role_resource` VALUES ('20', '3', '3');
INSERT INTO `role_resource` VALUES ('21', '3', '4');
INSERT INTO `role_resource` VALUES ('22', '3', '5');
INSERT INTO `role_resource` VALUES ('23', '3', '6');
INSERT INTO `role_resource` VALUES ('24', '3', '7');
INSERT INTO `role_resource` VALUES ('25', '3', '8');
INSERT INTO `role_resource` VALUES ('26', '3', '9');
INSERT INTO `role_resource` VALUES ('27', '3', '10');
INSERT INTO `role_resource` VALUES ('28', '3', '11');
INSERT INTO `role_resource` VALUES ('29', '3', '12');
INSERT INTO `role_resource` VALUES ('30', '3', '13');
INSERT INTO `role_resource` VALUES ('31', '3', '14');
INSERT INTO `role_resource` VALUES ('32', '3', '15');
INSERT INTO `role_resource` VALUES ('33', '3', '16');
INSERT INTO `role_resource` VALUES ('34', '3', '17');
INSERT INTO `role_resource` VALUES ('35', '2', '1');
INSERT INTO `role_resource` VALUES ('36', '2', '2');
INSERT INTO `role_resource` VALUES ('37', '2', '3');
INSERT INTO `role_resource` VALUES ('38', '2', '9');
INSERT INTO `role_resource` VALUES ('39', '2', '10');
INSERT INTO `role_resource` VALUES ('40', '3', '18');
INSERT INTO `role_resource` VALUES ('41', '1', '19');
INSERT INTO `role_resource` VALUES ('42', '2', '19');
INSERT INTO `role_resource` VALUES ('43', '3', '19');
INSERT INTO `role_resource` VALUES ('44', '1', '20');
INSERT INTO `role_resource` VALUES ('45', '2', '20');
INSERT INTO `role_resource` VALUES ('46', '3', '20');
INSERT INTO `role_resource` VALUES ('47', '1', '21');
INSERT INTO `role_resource` VALUES ('48', '3', '21');
INSERT INTO `role_resource` VALUES ('49', '1', '22');
INSERT INTO `role_resource` VALUES ('51', '3', '22');
INSERT INTO `role_resource` VALUES ('52', '1', '23');
INSERT INTO `role_resource` VALUES ('53', '3', '23');
INSERT INTO `role_resource` VALUES ('54', '1', '24');
INSERT INTO `role_resource` VALUES ('55', '3', '24');
INSERT INTO `role_resource` VALUES ('56', '1', '25');
INSERT INTO `role_resource` VALUES ('57', '3', '25');
INSERT INTO `role_resource` VALUES ('58', '1', '26');
INSERT INTO `role_resource` VALUES ('59', '2', '26');
INSERT INTO `role_resource` VALUES ('60', '3', '26');
INSERT INTO `role_resource` VALUES ('61', '1', '27');
INSERT INTO `role_resource` VALUES ('62', '3', '27');
INSERT INTO `role_resource` VALUES ('63', '1', '28');
INSERT INTO `role_resource` VALUES ('64', '3', '28');
INSERT INTO `role_resource` VALUES ('65', '1', '29');
INSERT INTO `role_resource` VALUES ('66', '3', '29');
INSERT INTO `role_resource` VALUES ('67', '1', '30');
INSERT INTO `role_resource` VALUES ('68', '3', '30');
INSERT INTO `role_resource` VALUES ('69', '1', '31');
INSERT INTO `role_resource` VALUES ('70', '3', '31');
INSERT INTO `role_resource` VALUES ('71', '1', '32');
INSERT INTO `role_resource` VALUES ('72', '3', '32');
INSERT INTO `role_resource` VALUES ('73', '1', '33');
INSERT INTO `role_resource` VALUES ('74', '3', '33');
INSERT INTO `role_resource` VALUES ('75', '1', '34');
INSERT INTO `role_resource` VALUES ('76', '3', '34');
INSERT INTO `role_resource` VALUES ('77', '1', '35');
INSERT INTO `role_resource` VALUES ('78', '3', '35');

-- ----------------------------
-- Table structure for tb_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_category`;
CREATE TABLE `tb_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`,`type`) USING BTREE,
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_category
-- ----------------------------
INSERT INTO `tb_category` VALUES ('1', 'T恤');
INSERT INTO `tb_category` VALUES ('8', '卫衣');
INSERT INTO `tb_category` VALUES ('3', '外套');
INSERT INTO `tb_category` VALUES ('6', '套装');
INSERT INTO `tb_category` VALUES ('13', '打底裤');
INSERT INTO `tb_category` VALUES ('12', '毛衣');
INSERT INTO `tb_category` VALUES ('2', '男衬衫');
INSERT INTO `tb_category` VALUES ('4', '短袖');
INSERT INTO `tb_category` VALUES ('10', '短裤');
INSERT INTO `tb_category` VALUES ('11', '羽绒服');
INSERT INTO `tb_category` VALUES ('9', '背心');
INSERT INTO `tb_category` VALUES ('5', '裙子');
INSERT INTO `tb_category` VALUES ('7', '长裤');

-- ----------------------------
-- Table structure for tb_clothes
-- ----------------------------
DROP TABLE IF EXISTS `tb_clothes`;
CREATE TABLE `tb_clothes` (
  `clothes_id` varchar(50) NOT NULL,
  `clothes_name` varchar(50) DEFAULT NULL,
  `clothes_type` varchar(30) DEFAULT NULL,
  `clothes_color` varchar(50) DEFAULT NULL,
  `clothes_size` varchar(5) DEFAULT NULL,
  `tag_price` double(25,2) DEFAULT NULL,
  `discount` int(5) DEFAULT '10',
  `discount_price` double(25,2) DEFAULT NULL,
  `clothes_material` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`clothes_id`) USING BTREE,
  KEY `clothes_type` (`clothes_type`) USING BTREE,
  CONSTRAINT `tb_clothes_ibfk_1` FOREIGN KEY (`clothes_type`) REFERENCES `tb_category` (`type`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_clothes
-- ----------------------------
INSERT INTO `tb_clothes` VALUES ('111', '夹克', '外套', '黑色', 'XL', '228.00', '9', '205.20', '牛仔');
INSERT INTO `tb_clothes` VALUES ('123', '四叶草T恤', 'T恤', '黑色', 'X', '128.00', '9', '115.20', '涤纶');
INSERT INTO `tb_clothes` VALUES ('123456', '条形衬衫', 'T恤', '黑白相间', 'L', '199.00', '9', '179.10', '亚麻');
INSERT INTO `tb_clothes` VALUES ('124', '羽绒夹克', '羽绒服', '黑色', 'S', '500.00', '10', '500.00', '鸭绒');
INSERT INTO `tb_clothes` VALUES ('125', '背带裙', '裙子', '红色色', 'M', '125.00', '8', '100.00', '牛仔');
INSERT INTO `tb_clothes` VALUES ('126', '吊带裙', '裙子', '白色', 'L', '100.00', '10', '90.00', '涤纶');
INSERT INTO `tb_clothes` VALUES ('127', '圆领毛衣', '毛衣', '灰色', 'M', '200.00', '10', '180.00', '羊毛');
INSERT INTO `tb_clothes` VALUES ('128', '圆领卫衣', '卫衣', '白色', 'L', '90.00', '10', '81.00', '纯棉');
INSERT INTO `tb_clothes` VALUES ('129', '牛仔外套', '外套', '蓝色', 'L', '300.00', '10', '270.00', '牛仔');
INSERT INTO `tb_clothes` VALUES ('130', '高腰短裤', '短裤', '蓝色', 'S', '50.00', '10', '45.00', '牛仔');
INSERT INTO `tb_clothes` VALUES ('131', '卡通短袖', '短袖', '白色', 'XL', '60.00', '10', '72.00', '纯棉');
INSERT INTO `tb_clothes` VALUES ('132', '小脚裤', '长裤', '黑色', 'M', '70.00', '10', '63.00', '牛仔');
INSERT INTO `tb_clothes` VALUES ('133', '阔腿裤', '长裤', '黑色', 'M', '150.00', '10', '135.00', '棉麻');
INSERT INTO `tb_clothes` VALUES ('134', '运动裤', '长裤', '黑色', 'L', '100.00', '10', '90.00', '羊毛');
INSERT INTO `tb_clothes` VALUES ('135', '棒球服', '外套', '黑白', 'M', '250.00', '10', '225.00', '丝绸');
INSERT INTO `tb_clothes` VALUES ('136', '休闲裤', '长裤', '浅灰', 'S', '100.00', '10', '90.00', '棉麻');
INSERT INTO `tb_clothes` VALUES ('137', '冲锋衣', '外套', '红色', 'S', '200.00', '10', '180.00', '涤纶');
INSERT INTO `tb_clothes` VALUES ('138', '男士运动背心', '背心', '白色', 'XL', '58.00', '10', '58.00', '涤纶');
INSERT INTO `tb_clothes` VALUES ('555', '高领毛衣', '毛衣', '白色', 'M', '228.00', '10', '228.00', '羊毛');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(25) DEFAULT NULL,
  `status` int(1) unsigned zerofill DEFAULT NULL,
  `build_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cashierId` varchar(25) DEFAULT NULL,
  `amount` double(30,1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES ('19', '13425145245', '1', '2019-04-23 22:23:59', '店长', '410.4');
INSERT INTO `tb_order` VALUES ('21', '137521246244', '1', '2019-04-23 22:23:03', 'test', '205.2');
INSERT INTO `tb_order` VALUES ('22', null, '0', '2019-05-08 12:36:44', 'test', '102.4');
INSERT INTO `tb_order` VALUES ('23', null, '0', '2019-05-08 12:39:20', 'test', '205.2');
INSERT INTO `tb_order` VALUES ('24', '137632541254', '0', '2019-05-08 19:33:52', 'test', '900.0');
INSERT INTO `tb_order` VALUES ('26', null, '0', '2019-05-08 23:09:30', 'test', '307.6');
INSERT INTO `tb_order` VALUES ('27', '137632541254', '0', '2019-03-01 19:33:52', 'test', '900.0');
INSERT INTO `tb_order` VALUES ('28', '139534201256', '0', '2019-02-01 21:54:06', 'test', '205.2');
INSERT INTO `tb_order` VALUES ('29', '13765325455', '1', '2019-06-07 09:50:18', 'test', '205.2');
INSERT INTO `tb_order` VALUES ('30', '', '1', '2019-06-07 09:56:22', 'test', '205.2');

-- ----------------------------
-- Table structure for tb_orderitem
-- ----------------------------
DROP TABLE IF EXISTS `tb_orderitem`;
CREATE TABLE `tb_orderitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) DEFAULT NULL,
  `clothesId` varchar(25) DEFAULT NULL,
  `clothesName` varchar(25) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  KEY `clothesId` (`clothesId`),
  CONSTRAINT `tb_orderitem_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `tb_order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tb_orderitem_ibfk_2` FOREIGN KEY (`clothesId`) REFERENCES `tb_clothes` (`clothes_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_orderitem
-- ----------------------------
INSERT INTO `tb_orderitem` VALUES ('8', '19', '111', '夹克', '2', '205.2', '410.4');
INSERT INTO `tb_orderitem` VALUES ('10', '21', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('11', '22', '123', '四叶草T恤', '1', '102.4', '102.4');
INSERT INTO `tb_orderitem` VALUES ('12', '23', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('13', '24', '129', '牛仔外套', '1', '270', '270');
INSERT INTO `tb_orderitem` VALUES ('14', '24', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('15', '24', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('16', '24', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('17', '19', '129', '牛仔外套', '1', '270', '270');
INSERT INTO `tb_orderitem` VALUES ('18', '21', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('19', null, '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('20', null, '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('21', '26', '123', '四叶草T恤', '1', '102.4', '102.4');
INSERT INTO `tb_orderitem` VALUES ('22', '26', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('23', '27', '129', '牛仔外套', '1', '270', '270');
INSERT INTO `tb_orderitem` VALUES ('24', '27', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('25', '27', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('26', '27', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('27', '24', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('28', '24', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('29', '24', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('30', '24', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('31', '24', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('32', '24', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('33', '24', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('34', '24', '133', '阔腿裤', '1', '135', '135');
INSERT INTO `tb_orderitem` VALUES ('35', '24', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('36', '24', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('37', '24', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('38', '24', '135', '棒球服', '2', '225', '450');
INSERT INTO `tb_orderitem` VALUES ('39', '28', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('40', '28', '130', '高腰短裤', '1', '45', '45');
INSERT INTO `tb_orderitem` VALUES ('41', '19', '129', '牛仔外套', '1', '270', '270');
INSERT INTO `tb_orderitem` VALUES ('42', '19', '129', '牛仔外套', '1', '270', '270');
INSERT INTO `tb_orderitem` VALUES ('43', '29', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('44', '30', '111', '夹克', '1', '205.2', '205.2');
INSERT INTO `tb_orderitem` VALUES ('45', null, '111', '夹克', '1', '205.2', '205.2');

-- ----------------------------
-- Table structure for tb_pay
-- ----------------------------
DROP TABLE IF EXISTS `tb_pay`;
CREATE TABLE `tb_pay` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) NOT NULL,
  `amount` double(25,1) NOT NULL,
  `payStyle` tinyint(5) NOT NULL,
  `payDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  CONSTRAINT `tb_pay_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `tb_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pay
-- ----------------------------
INSERT INTO `tb_pay` VALUES ('1', '22', '102.0', '1', '2019-05-08 12:36:51');
INSERT INTO `tb_pay` VALUES ('2', '23', '205.0', '3', '2019-05-08 12:39:20');
INSERT INTO `tb_pay` VALUES ('3', '26', '307.6', '4', '2019-05-08 23:09:30');
INSERT INTO `tb_pay` VALUES ('4', '27', '900.0', '4', '2019-05-09 13:14:05');
INSERT INTO `tb_pay` VALUES ('5', '24', '900.0', '4', '2019-05-09 13:50:03');

-- ----------------------------
-- Table structure for tb_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `permission` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='资源';

-- ----------------------------
-- Records of tb_resource
-- ----------------------------
INSERT INTO `tb_resource` VALUES ('1', '商品展示', '/clothes/find', 'clothes:find', '商品管理页面的商品展示');
INSERT INTO `tb_resource` VALUES ('2', '页面跳转', '/toClothesPage', 'page:toClothesPage', '跳转到商品管理页面');
INSERT INTO `tb_resource` VALUES ('3', '页面跳转', '/toCategoryPage', 'page:toCategoryPage', '跳转到类别管理页面');
INSERT INTO `tb_resource` VALUES ('4', '编辑商品信息', '/clothes/edit', 'clothes:edit', '商品管理页面的编辑商品信息');
INSERT INTO `tb_resource` VALUES ('5', '页面跳转', '/toIndex', 'page:toIndex', '跳转到主页');
INSERT INTO `tb_resource` VALUES ('6', '页面跳转', '/toAddUserPage', 'page:toAddUserPage', '跳转到用户管理页面');
INSERT INTO `tb_resource` VALUES ('7', '删除商品', '/clothes/delete', 'clothes:delete', '商品管理页面的删除商品信息');
INSERT INTO `tb_resource` VALUES ('8', '添加商品', '/clothes/add', 'clothes:add', '商品管理页面的添加商品');
INSERT INTO `tb_resource` VALUES ('9', '获取所有类型', '/category/find', 'category:find', '类型管理页面的获取所有类型');
INSERT INTO `tb_resource` VALUES ('10', '获取相应类型的衣服', '/category/getAll', 'category:getAll', '类型管理页面的获取相应类型的衣服');
INSERT INTO `tb_resource` VALUES ('11', '修改选中的类别', '/category/alterChoose', 'category:alterChoose', '类型管理页面的修改选中类别');
INSERT INTO `tb_resource` VALUES ('12', '新增类别', '/category/add', 'category:add', '类型管理页面的新增类别');
INSERT INTO `tb_resource` VALUES ('13', '添加用户', '/user/addUser', 'user:addUser', '用户管理页面的添加用户');
INSERT INTO `tb_resource` VALUES ('14', '查找用户', '/user/find', 'user:find', '用户管理页面的查找用户');
INSERT INTO `tb_resource` VALUES ('15', '页面跳转', '/user/toUserPage', 'page:toUserPage', '跳转到用户信息页面');
INSERT INTO `tb_resource` VALUES ('16', '批量删除商品', '/user/batchDelete', 'clothes:batchDelete', '商品管理页面的批量删除');
INSERT INTO `tb_resource` VALUES ('17', '删除用户', '/user/delete', 'user:delete', '用户管理信息的删除用户');
INSERT INTO `tb_resource` VALUES ('18', '添加销售记录', '/sale/addOrderItem', 'sale:addOrderItem', '前台销售添加销售记录');
INSERT INTO `tb_resource` VALUES ('19', '页面跳转', '/toSalePage', 'page:toSalePage', '跳转到销售页面');
INSERT INTO `tb_resource` VALUES ('20', '页面跳转', '/toTakeOrderPage', 'page:toTakeOrderPage', '跳转到挂单取单页面');
INSERT INTO `tb_resource` VALUES ('21', '页面跳转', '/toSaleDisplayPage', 'page:toSaleDisplayPage', '跳转到销售统计页面');
INSERT INTO `tb_resource` VALUES ('22', '页面跳转', '/toStockInPage', 'page:toStockInPage', '跳转到入库页面');
INSERT INTO `tb_resource` VALUES ('23', '页面跳转', '/user/toAlterUserPage', 'user:toAlterUserPage', '弹出用户修改页面弹窗');
INSERT INTO `tb_resource` VALUES ('24', '修改用户信息', '/user/editUser', 'user:editUser', '用户管理页面的修改用户');
INSERT INTO `tb_resource` VALUES ('25', '修改商品弹窗', '/clothes/toAlterClothesPage', 'clothes:toAlterClothesPage', '商品管理页面的修改商品弹窗');
INSERT INTO `tb_resource` VALUES ('26', '查询挂单订单', '/order/findTakeOrder', 'order:findTakeOrder', '挂单页面的查询挂单订单');
INSERT INTO `tb_resource` VALUES ('27', '销售统计页面的获取所有类别', '/saleStatement/getCategory', 'saleStatement:getCategory', '销售统计页面的获取所有类别');
INSERT INTO `tb_resource` VALUES ('28', '根据类别获取服装', '/saleStatement/getClothesByCategory', 'saleStatement:getClothesByCategory', '销售统计页面的根据类别获取服装');
INSERT INTO `tb_resource` VALUES ('29', '根据起始时间获取指定种类服装的销售量', '/saleStatement/saleVolumeDateRange', 'saleStatement:saleVolumeDateRange', '销售统计页面的根据起始时间获取指定种类服装的销售量');
INSERT INTO `tb_resource` VALUES ('30', '获取指定服装的销售数量', '/saleStatement/saleVolumeMonthRange', 'saleStatement:saleVolumeMonthRange', '销售统计页面获取指定服装的销售数量');
INSERT INTO `tb_resource` VALUES ('31', '转换为季度范围', '/saleStatement/turnoverMonthRange', 'saleStatement:turnoverMonthRange', '销售统计页面的转换为季度范围');
INSERT INTO `tb_resource` VALUES ('32', '根据服装编号查找服装表', '/stock/selectClothesOne', 'stock:selectClothesOne', '商品入库页面根据服装编号查找服装表');
INSERT INTO `tb_resource` VALUES ('33', '新商品入库', '/stock/stockIn', 'stock:stockIn', '商品入库页面新商品入库');
INSERT INTO `tb_resource` VALUES ('34', '已存在的商品入库', '/stock/alterStock', 'stock:alterStock', '商品入库页面已存在的商品入库');
INSERT INTO `tb_resource` VALUES ('35', '根据商品编号查找同类型的商品库存', '/stock/searchStock', 'stock:searchStock', '商品入库页面根据商品编号查找同类型的商品库存');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(25) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES ('1', '经理', '经理');
INSERT INTO `tb_role` VALUES ('2', '收银员', '收银员');
INSERT INTO `tb_role` VALUES ('3', '店长', '店长');

-- ----------------------------
-- Table structure for tb_stock
-- ----------------------------
DROP TABLE IF EXISTS `tb_stock`;
CREATE TABLE `tb_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clothes_id` varchar(50) NOT NULL,
  `stock` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tb_stock_ibfk_1` (`clothes_id`) USING BTREE,
  CONSTRAINT `tb_stock_ibfk_1` FOREIGN KEY (`clothes_id`) REFERENCES `tb_clothes` (`clothes_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_stock
-- ----------------------------
INSERT INTO `tb_stock` VALUES ('1', '123', '10');
INSERT INTO `tb_stock` VALUES ('2', '124', '20');
INSERT INTO `tb_stock` VALUES ('3', '125', '23');
INSERT INTO `tb_stock` VALUES ('4', '126', '50');
INSERT INTO `tb_stock` VALUES ('5', '127', '12');
INSERT INTO `tb_stock` VALUES ('6', '128', '30');
INSERT INTO `tb_stock` VALUES ('7', '129', '22');
INSERT INTO `tb_stock` VALUES ('8', '130', '34');
INSERT INTO `tb_stock` VALUES ('9', '131', '22');
INSERT INTO `tb_stock` VALUES ('10', '132', '44');
INSERT INTO `tb_stock` VALUES ('11', '133', '51');
INSERT INTO `tb_stock` VALUES ('12', '134', '14');
INSERT INTO `tb_stock` VALUES ('13', '135', '18');
INSERT INTO `tb_stock` VALUES ('14', '136', '23');
INSERT INTO `tb_stock` VALUES ('15', '137', '21');
INSERT INTO `tb_stock` VALUES ('16', '111', '18');
INSERT INTO `tb_stock` VALUES ('17', '123456', '15');
INSERT INTO `tb_stock` VALUES ('18', '555', '7');
INSERT INTO `tb_stock` VALUES ('19', '138', '1');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `sex` varchar(10) NOT NULL DEFAULT '0',
  `age` tinyint(2) DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`loginname`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'manager', '经理', 'ada1be43ad606a0131b74c44b88c2dd2', '1', '25', '0', '2019-04-09 09:56:44', '18707173376');
INSERT INTO `tb_user` VALUES ('2', 'test', '店长', 'b0ce72ed0b24728785094ef90b6d00c3', '1', '25', '0', '2019-04-09 15:40:09', '18707173376');
INSERT INTO `tb_user` VALUES ('12', 'testSaler3', '收银员C', 'f2d932da9b08704168c508973abaa13a', '1', '21', '0', '2019-04-09 15:49:40', '137652314203');
INSERT INTO `tb_user` VALUES ('13', 'testSaler2', '收银员B', 'ce313071dc7d49eb37d62e2de2d579c9', '1', '22', '0', '2019-04-09 15:49:31', '152631204521');
INSERT INTO `tb_user` VALUES ('14', 'testSaler1', '收银员A', '047cb18a56d1559d741ee8f80f0773f0', '1', '22', '0', '2019-04-09 15:47:31', '154262310256');
INSERT INTO `tb_user` VALUES ('16', 'testManage1', '经理A', '59c385f45787da5fa2a29ceaabad029f', '1', '28', '0', '2019-04-09 17:35:47', '132422562102');
INSERT INTO `tb_user` VALUES ('17', 'rr', '小林', '96d84443035c33b4cfafbf919e5d6bdb', '1', '24', '0', '2019-05-07 21:57:35', '187071733');
INSERT INTO `tb_user` VALUES ('18', 'testSaler4', '收银员D', '487ba4526df2ee67fe7d7ddfa1df6700', '1', '23', '0', '2019-05-03 17:41:46', '13756241203');
INSERT INTO `tb_user` VALUES ('19', '001', '李四', 'a71ea0d33b002a81479eabcedbce5e34', '1', '20', '0', '2019-06-07 22:16:37', '15984215204');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '2', '3');
INSERT INTO `user_role` VALUES ('2', '1', '1');
INSERT INTO `user_role` VALUES ('8', '12', '2');
INSERT INTO `user_role` VALUES ('9', '13', '2');
INSERT INTO `user_role` VALUES ('10', '14', '2');
INSERT INTO `user_role` VALUES ('11', null, '2');
INSERT INTO `user_role` VALUES ('12', '16', '1');
INSERT INTO `user_role` VALUES ('13', '17', '2');
INSERT INTO `user_role` VALUES ('14', '18', '2');
INSERT INTO `user_role` VALUES ('15', '19', '2');

-- ----------------------------
-- Event structure for a
-- ----------------------------
DROP EVENT IF EXISTS `a`;
DELIMITER ;;
CREATE DEFINER=`skip-grants user`@`skip-grants host` EVENT `a` ON SCHEDULE AT '2019-04-04 10:51:52' ON COMPLETION NOT PRESERVE ENABLE DO truncate table tb_resource
;;
DELIMITER ;
